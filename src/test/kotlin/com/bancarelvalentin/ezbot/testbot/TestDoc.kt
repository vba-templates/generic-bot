package com.bancarelvalentin.ezbot.testbot

import com.bancarelvalentin.ezbot.process.doc.Doc

interface TestDoc : Doc {
    override val rawName: String
        get() = this.javaClass.simpleName
    override val rawDesc: String
        get() = ""
}
