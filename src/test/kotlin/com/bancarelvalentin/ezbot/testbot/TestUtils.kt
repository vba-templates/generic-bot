package com.bancarelvalentin.ezbot.testbot

import com.bancarelvalentin.ezbot.Orchestrator
import com.bancarelvalentin.ezbot.config.ConfigHandler
import com.bancarelvalentin.ezbot.logger.LogCategoriesEnum
import com.bancarelvalentin.ezbot.logger.Logger
import com.bancarelvalentin.ezbot.utils.AsyncUtils
import net.dv8tion.jda.api.JDA
import net.dv8tion.jda.api.entities.MessageEmbed
import java.time.Duration
import java.util.stream.Stream

class TestUtils {

    companion object {
        val logger = Logger(TestUtils::class.java)

        fun allTestComands(): Stream<TestCommand> {
            return Orchestrator.getCommands(TestCommand::class.java).stream()
        }

        fun sendInAllDefaultChannels(gateway: JDA, embed: MessageEmbed) = gateway.guilds
            .filter { it.idLong == 677258457778356237 }
            .forEach { it.defaultChannel?.sendMessageEmbeds(embed)?.queue() }

        fun execAll(gateway: JDA, cmds: ArrayList<Pair<String, String>>) {
            val channel = gateway.getTextChannelById((ConfigHandler.config as TestConfig).channelIdToExecuteTest)!!
            for ((i, pair) in cmds.withIndex()) {
                val cmd = pair.first
                val result = "\t => ${pair.second}"
                logger.info("Executing auto test $cmd.", LogCategoriesEnum.INITIAL_STARTUP)
                AsyncUtils.delay(Duration.ofSeconds(i.toLong())) {
                    channel.sendMessage(cmd).queue()
                    channel.sendMessage(result).queue()
                }
            }
        }
    }
}
