package com.bancarelvalentin.ezbot.testbot.background

import com.bancarelvalentin.ezbot.Orchestrator
import com.bancarelvalentin.ezbot.process.EventListener
import com.bancarelvalentin.ezbot.testbot.TestBackgroundProcess
import com.bancarelvalentin.ezbot.testbot.command.TestAutoCompletion
import com.bancarelvalentin.ezbot.utils.SlashCommandRegistrationUtils
import net.dv8tion.jda.api.events.ReadyEvent

@Suppress("unused")
class TestRegisterSlashCommandsOnReadyProcess : TestBackgroundProcess() {

    override val rawName: String = "Register slash commands on ready"
    override val rawDesc: String = rawName

    override fun getListeners(): Array<EventListener> {
        return emptyArray()
    }

    override var realReadyListeners: ((ReadyEvent) -> Unit)? = {
        SlashCommandRegistrationUtils.register(gateway, Orchestrator.getCommand(TestAutoCompletion::class.java))
    }
}
