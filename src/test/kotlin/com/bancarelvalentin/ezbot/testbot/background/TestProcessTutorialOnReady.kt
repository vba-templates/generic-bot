package com.bancarelvalentin.ezbot.testbot.background

import com.bancarelvalentin.ezbot.config.ConfigHandler
import com.bancarelvalentin.ezbot.process.EventListener
import com.bancarelvalentin.ezbot.process.Process
import com.bancarelvalentin.ezbot.process.command.enums.STATUS
import com.bancarelvalentin.ezbot.testbot.TestBackgroundProcess
import com.bancarelvalentin.ezbot.testbot.TestConfig
import com.bancarelvalentin.ezbot.testbot.TestUtils
import com.bancarelvalentin.ezbot.utils.ChannelUtils
import com.bancarelvalentin.ezbot.utils.Constants
import net.dv8tion.jda.api.EmbedBuilder
import net.dv8tion.jda.api.events.ReadyEvent
import java.util.stream.Collectors

@Suppress("unused")
class TestProcessTutorialOnReady : TestBackgroundProcess() {

    override fun getListeners(): Array<EventListener> {
        return arrayOf(TestMsgOnReadyProcessListener(this))
    }

    override val rawName: String = "Test process details"
    override val rawDesc: String = "Message a detailed testing process on ready"

    class TestMsgOnReadyProcessListener(process: Process) : EventListener(process) {
        override fun onReady(event: ReadyEvent) {
            (ConfigHandler.config as TestConfig).channelIdToEmpty.forEach { id -> ChannelUtils.emptyChannel(gateway.getTextChannelById(id)!!) }

            val embedBuilder = EmbedBuilder()
                .setColor(ConfigHandler.config.defaultColor)
                .setTitle("Execute all the below commands (prefixing with `${ConfigHandler.config.prefixes[0]}`) and check result are as excpected.")
                .setDescription(
                    "You should also check:" +
                            "\nif slash commands are working with autocomplete;" +
                            "\nthat lazy commands works;" +
                            "\nthat the bot confirmed the cron worked via your DMs;" +
                            "\nthat the bot confirmed the extraOnReadyListeners worked via your DMs;" +
                            "\nthat the DJ works as intended;" +
                            "\nand that two stacktrace are generated in the error log channel." +
                            "\n\n**You can print all test commands in a less verbose way with `.pa`**"
                )

            val cmdsToRun = ArrayList<Pair<String, String>>()
            TestUtils.allTestComands()
                .forEach { testProcess ->
                    val reminderMessageContent = testProcess.commandsToRun().toList().stream()
                        .map {
                            val cmd = "${ConfigHandler.config.prefixes[0]}${it.syntax}"
                            cmdsToRun.add(Pair(cmd, it.expectedResults))
                            val emoji = when (it.expectedStatus) {
                                STATUS.SUCCESS -> Constants.EMOTE_SUCCESS
                                STATUS.WARNING -> Constants.EMOTE_WARNING
                                STATUS.ERROR -> Constants.EMOTE_ERROR
                            }
                            "`$cmd` => $emoji - ${it.expectedResults}"
                        }
                        .collect(Collectors.joining("\n"))
                    embedBuilder.addField(testProcess.rawName, reminderMessageContent, false)
                }
            TestUtils.sendInAllDefaultChannels(gateway, embedBuilder.build())
            if (System.getenv("AUTO_TEST")?.equals("true") == true) {
                TestUtils.execAll(gateway, cmdsToRun)
            }
        }
    }
}
