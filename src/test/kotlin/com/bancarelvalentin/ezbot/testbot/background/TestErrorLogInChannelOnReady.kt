package com.bancarelvalentin.ezbot.testbot.background

import com.bancarelvalentin.ezbot.exception.DiscordErrorLogger
import com.bancarelvalentin.ezbot.process.EventListener
import com.bancarelvalentin.ezbot.process.Process
import com.bancarelvalentin.ezbot.testbot.DummyImplementationException
import com.bancarelvalentin.ezbot.testbot.DummyUserException
import com.bancarelvalentin.ezbot.testbot.TestBackgroundProcess
import com.bancarelvalentin.ezbot.utils.AsyncUtils
import net.dv8tion.jda.api.events.ReadyEvent
import java.time.Duration

@Suppress("unused")
class TestErrorLogInChannelOnReady : TestBackgroundProcess() {

    override fun getListeners(): Array<EventListener> {
        return arrayOf(TestErrorLogInChannelOnReadyListener(this))
    }

    override val rawName: String = "Test error log"
    override val rawDesc: String = "Send two stacktrace in the error log channel; a user error and an implementation error"

    class TestErrorLogInChannelOnReadyListener(process: Process) : EventListener(process) {
        override fun onReady(event: ReadyEvent) {
            AsyncUtils.delay(Duration.ofSeconds(20)) {
                DiscordErrorLogger.handle(DummyImplementationException())
                DiscordErrorLogger.handle(DummyUserException())
            }
        }
    }
}
