package com.bancarelvalentin.ezbot.testbot

import com.bancarelvalentin.ezbot.exception.usererrors.AbstractUserException

class DummyUserException(message: String? = null) : AbstractUserException(message ?: "Dummy exception")
