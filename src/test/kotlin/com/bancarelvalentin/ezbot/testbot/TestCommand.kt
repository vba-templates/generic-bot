package com.bancarelvalentin.ezbot.testbot

import com.bancarelvalentin.ezbot.process.command.Command

abstract class TestCommand : Command(), TestInstance {

    abstract fun commandsToRun(): Array<AutoTest>
}
