package com.bancarelvalentin.ezbot.testbot

import com.bancarelvalentin.ezbot.process.command.enums.STATUS

class AutoTest(val syntax: String, val expectedStatus: STATUS, val expectedResults: String)
