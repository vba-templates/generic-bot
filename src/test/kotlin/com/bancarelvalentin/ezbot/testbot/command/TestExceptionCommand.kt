package com.bancarelvalentin.ezbot.testbot.command

import com.bancarelvalentin.ezbot.process.command.enums.STATUS
import com.bancarelvalentin.ezbot.process.command.param.CommandParam
import com.bancarelvalentin.ezbot.process.command.param.StringCommandParam
import com.bancarelvalentin.ezbot.process.command.request.CommandRequest
import com.bancarelvalentin.ezbot.process.command.response.CommandResponse
import com.bancarelvalentin.ezbot.testbot.AutoTest
import com.bancarelvalentin.ezbot.testbot.DummyImplementationException
import com.bancarelvalentin.ezbot.testbot.DummyUserException
import com.bancarelvalentin.ezbot.testbot.TestCommand
import java.util.function.BiConsumer

@Suppress("unused")
class TestExceptionCommand : TestCommand() {

    override val patterns = arrayOf("test_error", "te")
    override val paramsClasses: Array<Class<out CommandParam<out Any?>>> = arrayOf(ErrorMessageParam::class.java)

    class ErrorMessageParam : StringCommandParam() {
        override val optional = true
        override val rawName = "Error type to return"
        override val rawDesc = "user for a UserException, impl for an ImplementationException or none for a RuntimeException"
    }

    override val logic = BiConsumer { request: CommandRequest, _: CommandResponse ->
        when (request.getParamString(0)) {
            "user" -> throw DummyUserException()
            "impl" -> throw DummyImplementationException()
            else -> throw RuntimeException()
        }
    }

    override fun commandsToRun(): Array<AutoTest> {
        return arrayOf(
            AutoTest("te user", STATUS.WARNING, "react to command as warning"),
            AutoTest("te impl", STATUS.ERROR, "react to command as error and reply with a stacktrace"),
            AutoTest("te", STATUS.ERROR, "react to command as error and reply with a stacktrace"),
        )
    }
}
