package com.bancarelvalentin.ezbot.testbot.command.params

import com.bancarelvalentin.ezbot.process.command.param.UserCommandParam

open class TestUserRequiredParam : UserCommandParam() {
    override val rawName: String = this.javaClass.simpleName
    override val rawDesc = ""
}
