package com.bancarelvalentin.ezbot.testbot.command.params

import com.bancarelvalentin.ezbot.process.command.param.IntCommandParam

open class TestIntRequiredParam : IntCommandParam() {
    override val rawName: String = this.javaClass.simpleName
    override val rawDesc = ""
}
