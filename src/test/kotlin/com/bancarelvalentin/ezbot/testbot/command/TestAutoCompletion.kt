package com.bancarelvalentin.ezbot.testbot.command

import com.bancarelvalentin.ezbot.process.command.enums.STATUS
import com.bancarelvalentin.ezbot.process.command.param.CommandParam
import com.bancarelvalentin.ezbot.process.command.request.CommandRequest
import com.bancarelvalentin.ezbot.process.command.response.CommandResponse
import com.bancarelvalentin.ezbot.testbot.AutoTest
import com.bancarelvalentin.ezbot.testbot.TestCommand
import com.bancarelvalentin.ezbot.testbot.command.params.TestAutoCompleteParam
import java.util.function.BiConsumer

@Suppress("unused")
class TestAutoCompletion : TestCommand() {

    override val patterns = arrayOf("test_autocomplete", "tac")
    override val paramsClasses: Array<Class<out CommandParam<out Any?>>> = arrayOf(TestAutoCompleteParam::class.java)

    override val logic = BiConsumer { request: CommandRequest, _: CommandResponse -> request.forceReply("you chose ${request.params[0].get()}") }

    override fun commandsToRun(): Array<AutoTest> {
        return arrayOf(AutoTest("test_autocomplete", STATUS.SUCCESS, "You should test this via slash comands. The param should be autocompleted."))
    }
}
