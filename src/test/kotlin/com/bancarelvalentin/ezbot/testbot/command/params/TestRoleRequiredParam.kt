package com.bancarelvalentin.ezbot.testbot.command.params

import com.bancarelvalentin.ezbot.process.command.param.RoleCommandParam

open class TestRoleRequiredParam : RoleCommandParam() {
    override val rawName: String = this.javaClass.simpleName
    override val rawDesc = ""
}
