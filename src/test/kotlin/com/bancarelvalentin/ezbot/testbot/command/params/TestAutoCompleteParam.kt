package com.bancarelvalentin.ezbot.testbot.command.params

import com.bancarelvalentin.ezbot.process.command.param.StringCommandParam
import net.dv8tion.jda.api.interactions.commands.Command

open class TestAutoCompleteParam : StringCommandParam() {
    override val rawName: String = this.javaClass.simpleName
    override val rawDesc = ""
    override val choices = listOf(Command.Choice("option A", "A"), Command.Choice("option B", "B"))
}
