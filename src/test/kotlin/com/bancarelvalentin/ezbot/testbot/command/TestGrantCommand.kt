package com.bancarelvalentin.ezbot.testbot.command

import com.bancarelvalentin.ezbot.process.command.enums.STATUS
import com.bancarelvalentin.ezbot.process.command.request.CommandRequest
import com.bancarelvalentin.ezbot.process.command.response.CommandResponse
import com.bancarelvalentin.ezbot.testbot.AutoTest
import com.bancarelvalentin.ezbot.testbot.TestCommand
import java.util.function.BiConsumer

@Suppress("unused")
class TestGrantCommand : TestCommand() {

    override val whitelistUserIds = arrayOf(0L)

    override val patterns = arrayOf("test_grants", "tg")

    override val logic = BiConsumer { _: CommandRequest, _: CommandResponse ->
    }

    override fun commandsToRun(): Array<AutoTest> {
        return arrayOf(
            AutoTest("tg", STATUS.WARNING, "react to command as warning and reply with a warning saying you're not granted"),
        )
    }
}
