package com.bancarelvalentin.ezbot.testbot.command

import com.bancarelvalentin.ezbot.config.ConfigHandler
import com.bancarelvalentin.ezbot.process.command.enums.STATUS
import com.bancarelvalentin.ezbot.process.command.request.ChatCommandRequest
import com.bancarelvalentin.ezbot.process.command.request.CommandRequest
import com.bancarelvalentin.ezbot.process.command.response.CommandResponse
import com.bancarelvalentin.ezbot.testbot.AutoTest
import com.bancarelvalentin.ezbot.testbot.TestCommand
import com.bancarelvalentin.ezbot.testbot.TestConfig
import com.bancarelvalentin.ezbot.utils.EmojiEnum
import com.bancarelvalentin.ezbot.utils.FormatingUtils
import net.dv8tion.jda.api.EmbedBuilder
import java.util.Date
import java.util.function.BiConsumer

@Suppress("unused")
class TestAllFormatCommand : TestCommand() {

    override val patterns = arrayOf("test_all_format", "taf")

    override val logic = BiConsumer { request: CommandRequest, response: CommandResponse ->
        val builder = EmbedBuilder()
            .addField("formatUserId", FormatingUtils.formatUserId((ConfigHandler.config as TestConfig).dummyUserId), false)
            .addField("formatUserIdAsNickname", FormatingUtils.formatUserIdAsNickname((ConfigHandler.config as TestConfig).dummyUserId), false)
            .addField("formatChannelId", FormatingUtils.formatChannelId((ConfigHandler.config as TestConfig).dummyChannelId), false)
            .addField("formatRoleId", FormatingUtils.formatRoleId((ConfigHandler.config as TestConfig).dummyroleId), false)
            .addField("getStandardEmoji", EmojiEnum.SMIRKING_FACE.tounicode(), false)
            .addField("formatCustomEmoji", FormatingUtils.formatCustomEmoji((ConfigHandler.config as TestConfig).dummyEmojiId, "test"), false)
            .addField("getCustomEmoji", FormatingUtils.getCustomEmoji("test", request.guild!!), false)
            .addField("getCustomEmoji", FormatingUtils.getCustomEmoji((ConfigHandler.config as TestConfig).dummyEmojiId, request.guild!!), false)
            .addField("formatDate", FormatingUtils.formatDate(Date()), false)
            .addField("formatDate (relative)", FormatingUtils.formatDate(Date(), FormatingUtils.TIMESTAMP_FORMAT.RELATIVE_TIME), false)
        if (request is ChatCommandRequest) {
            builder.addField("messageToLink", FormatingUtils.messageToLink(request.event), false)
        }
        response.embedOverride = builder
    }

    override fun commandsToRun(): Array<AutoTest> {
        return arrayOf(
            AutoTest("taf", STATUS.SUCCESS, "Return a dummy list of all formatable entities")
        )
    }
}
