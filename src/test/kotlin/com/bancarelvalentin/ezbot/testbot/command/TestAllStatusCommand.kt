package com.bancarelvalentin.ezbot.testbot.command

import com.bancarelvalentin.ezbot.process.command.enums.STATUS
import com.bancarelvalentin.ezbot.process.command.param.CommandParam
import com.bancarelvalentin.ezbot.process.command.param.StringCommandParam
import com.bancarelvalentin.ezbot.process.command.request.CommandRequest
import com.bancarelvalentin.ezbot.process.command.response.CommandResponse
import com.bancarelvalentin.ezbot.testbot.AutoTest
import com.bancarelvalentin.ezbot.testbot.DummyImplementationException
import com.bancarelvalentin.ezbot.testbot.TestCommand
import java.util.function.BiConsumer

@Suppress("unused")
class TestAllStatusCommand : TestCommand() {

    override val paramsClasses: Array<Class<out CommandParam<out Any?>>> = arrayOf(StatusParam::class.java, MessageParam::class.java)

    class StatusParam : StringCommandParam() {
        override val optional = true
        override val rawName = "Status to return"
        override val rawDesc = "The status to reply with to the command with"
    }

    class MessageParam : StringCommandParam() {
        override val optional = true
        override val rawName = "Message to return"
        override val rawDesc = "The message to reply with to the command"
    }

    override val patterns = arrayOf("test_status", "ts")

    override val logic = BiConsumer { request: CommandRequest, response: CommandResponse ->
        if (request.hasParam(1)) {
            val status = request.joinParams(1)
            response.message = status
        }
        when (request.getParamString(0)) {
            "s" -> {
                response.responseStatus = STATUS.SUCCESS
            }
            "w" -> {
                response.responseStatus = STATUS.WARNING
            }
            "e" -> {
                response.responseStatus = STATUS.ERROR
            }
            else -> throw DummyImplementationException()
        }
    }

    override fun commandsToRun(): Array<AutoTest> {
        return arrayOf(
            AutoTest("ts s", STATUS.SUCCESS, "just react to command as success"),
            AutoTest("ts w", STATUS.WARNING, "just react to command as warning"),
            AutoTest("ts e", STATUS.ERROR, "just react to command as error"),
            AutoTest("ts s Salut mes p'tits loulou", STATUS.SUCCESS, "react to command as success and reply with \"Salut mes p'tits loulou\""),
            AutoTest("ts w Salut mes p'tits loulou", STATUS.WARNING, "react to command as warning and reply with \"Salut mes p'tits loulou\""),
            AutoTest("ts e Salut mes p'tits loulou", STATUS.ERROR, "react to command as error and reply with \"Salut mes p'tits loulou\""),
        )
    }
}
