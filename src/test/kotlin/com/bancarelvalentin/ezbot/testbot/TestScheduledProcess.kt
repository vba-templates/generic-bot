package com.bancarelvalentin.ezbot.testbot

import com.bancarelvalentin.ezbot.process.scheduled.ScheduledProcess

abstract class TestScheduledProcess : ScheduledProcess(), TestInstance
