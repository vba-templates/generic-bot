package com.bancarelvalentin.ezbot.process

import net.dv8tion.jda.api.events.ReadyEvent

abstract class EventListenerProcess : Process() {

    /**
     * Called after the post own ready event is called
     */
    open var realReadyListeners: ((ReadyEvent) -> Unit)? = null

    abstract fun getListeners(): Array<EventListener>
}
