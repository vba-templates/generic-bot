package com.bancarelvalentin.ezbot.process.doc

import com.bancarelvalentin.ezbot.config.ConfigHandler
import net.dv8tion.jda.api.EmbedBuilder

@Suppress("SameReturnValue")
interface Doc {

    /**
     * The command name to be shown in doc
     */
    val rawName: String

    /**
     * The command description to be shown in doc
     */
    val rawDesc: String

    @Suppress("LeakingThis")
    fun generateDocName(): String {
        return rawName
    }

    @Suppress("LeakingThis")
    fun generateDocDesc(): String {
        return rawDesc
    }

    @Suppress("LeakingThis")
    fun generateDocDescShort(): String {
        return generateDocDesc()
    }

    /**
     * Return a pair with a short title and a short description of this Process
     */
    fun toEmbedField(): Pair<String, String> {
        return Pair(generateDocName(), generateDocDescShort())
    }

    /**
     * Return an embed extensively describing this Process
     */
    fun getDocEmbedBuilder(): EmbedBuilder {
        val builder = EmbedBuilder()
            .setColor(ConfigHandler.config.defaultColor)
            .setTitle(generateDocName())
            .setDescription(generateDocDesc())
        if (ConfigHandler.config.version != null) {
            builder.setFooter(ConfigHandler.config.version ?: "(version not set)")
        }
        return builder
    }
}
