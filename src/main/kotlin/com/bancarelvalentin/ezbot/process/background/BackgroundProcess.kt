package com.bancarelvalentin.ezbot.process.background

import com.bancarelvalentin.ezbot.process.EventListenerProcess
import com.bancarelvalentin.ezbot.process.doc.BackgroundProcessDoc

abstract class BackgroundProcess : BackgroundProcessDoc, EventListenerProcess()
