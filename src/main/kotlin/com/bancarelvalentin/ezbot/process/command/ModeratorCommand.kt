package com.bancarelvalentin.ezbot.process.command

import com.bancarelvalentin.ezbot.config.ConfigHandler

abstract class ModeratorCommand : Command() {
    override val whitelistUserIds = ConfigHandler.config.moderatorsIds
    override val whitelistRoleIds = ConfigHandler.config.moderatorsRolesIds
}
