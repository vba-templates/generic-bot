package com.bancarelvalentin.ezbot.process.command.handlers

import com.bancarelvalentin.ezbot.config.ConfigHandler
import com.bancarelvalentin.ezbot.exception.usererrors.MissingRequiredParameterException
import com.bancarelvalentin.ezbot.exception.usererrors.WrongTypeParameterException
import com.bancarelvalentin.ezbot.logger.LogCategoriesEnum
import com.bancarelvalentin.ezbot.logger.LogCategory
import com.bancarelvalentin.ezbot.process.command.Command
import com.bancarelvalentin.ezbot.process.command.param.CommandParam
import com.bancarelvalentin.ezbot.process.command.param.ExtraParam
import com.bancarelvalentin.ezbot.process.command.request.CommandRequest
import com.bancarelvalentin.ezbot.process.command.request.SimulatedCommandRequest
import net.dv8tion.jda.api.entities.Guild
import net.dv8tion.jda.api.entities.MessageChannel
import net.dv8tion.jda.api.entities.TextChannel
import net.dv8tion.jda.api.entities.User

open class SimulatedCommandHandler(commandd: Command, val guildd: Guild, val channell: TextChannel, val userr: User, val params: Array<out String>) : AbstractCommandHandler(commandd) {

    override val logCat: LogCategory = LogCategoriesEnum.CHAT_COMMAND

    override fun getGuild(): Guild {
        return guildd
    }

    override fun getReplyChannel(): MessageChannel {
        return channell
    }

    override fun getUser(): User {
        return userr
    }

    override fun getRequest(): CommandRequest? {
        return SimulatedCommandRequest(command, guildd, channell, userr, command.getRawTriggers()[0], params, ConfigHandler.config.prefixes[0])
    }

    override fun parseParams(request: CommandRequest): Array<CommandParam<out Any?>> {
        val args: Array<String> = request.rawParams.map { it as String }.toTypedArray()
        val params = ArrayList<CommandParam<out Any?>>()
        if (this.command.paramsClasses.isNotEmpty()) {
            val max = maxOf(this.command.paramsClasses.size, args.size)
            for (i in 0 until max) {
                val clazz: Class<out CommandParam<out Any?>> = try {
                    this.command.paramsClasses[i]
                } catch (e: IndexOutOfBoundsException) {
                    ExtraParam::class.java
                }

                val arg = try {
                    args[i]
                } catch (e: IndexOutOfBoundsException) {
                    null
                }

                val paramInstance = clazz.getDeclaredConstructor().newInstance()
                if (arg != null) {
                    try {
                        paramInstance.setRaw(arg, gateway)
                    } catch (e: Exception) {
                        throw WrongTypeParameterException(paramInstance.rawName, paramInstance.docType, i + 1)
                    }
                }
                if (paramInstance.get() == null && !paramInstance.optional) {
                    throw MissingRequiredParameterException(paramInstance.rawName, i + 1)
                } else {
                    params.add(paramInstance)
                }
            }
        }
        return params.toTypedArray()
    }
}
