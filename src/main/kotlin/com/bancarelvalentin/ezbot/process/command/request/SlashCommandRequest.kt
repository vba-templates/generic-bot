package com.bancarelvalentin.ezbot.process.command.request

import com.bancarelvalentin.ezbot.process.command.Command
import net.dv8tion.jda.api.entities.Message
import net.dv8tion.jda.api.entities.MessageChannel
import net.dv8tion.jda.api.entities.MessageEmbed
import net.dv8tion.jda.api.events.interaction.SlashCommandEvent
import net.dv8tion.jda.api.interactions.InteractionHook

@Suppress("unused")
class SlashCommandRequest(handler: Command, channel: MessageChannel, val event: SlashCommandEvent, command: String, rawParams: Array<out Any>) :
    CommandRequest(handler, event.guild, channel, event.user, command, rawParams) {

    override val prefix = "/"
    override val forceReplyOnEmpty = true

    var hook: InteractionHook = event.deferReply(true).setEphemeral(true).complete()

    override fun forceReply(str: String) {
        hook.editOriginal(str).queue { reply = it }
    }

    override fun forceReply(str: Message) {
        hook.editOriginal(str).queue { reply = it }
    }

    override fun forceReply(embed: MessageEmbed) {
        hook.editOriginalEmbeds(embed).queue { reply = it }
    }
}
