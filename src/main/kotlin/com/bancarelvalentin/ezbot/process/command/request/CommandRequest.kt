package com.bancarelvalentin.ezbot.process.command.request

import com.bancarelvalentin.ezbot.process.command.Command
import com.bancarelvalentin.ezbot.process.command.param.ChannelCommandParam
import com.bancarelvalentin.ezbot.process.command.param.CommandParam
import com.bancarelvalentin.ezbot.process.command.param.IntCommandParam
import com.bancarelvalentin.ezbot.process.command.param.NumberCommandParam
import com.bancarelvalentin.ezbot.process.command.param.RoleCommandParam
import com.bancarelvalentin.ezbot.process.command.param.StringCommandParam
import com.bancarelvalentin.ezbot.process.command.param.UserCommandParam
import net.dv8tion.jda.api.entities.Emote
import net.dv8tion.jda.api.entities.Guild
import net.dv8tion.jda.api.entities.Member
import net.dv8tion.jda.api.entities.Message
import net.dv8tion.jda.api.entities.MessageChannel
import net.dv8tion.jda.api.entities.MessageEmbed
import net.dv8tion.jda.api.entities.Role
import net.dv8tion.jda.api.entities.User
import java.util.stream.Collectors

@Suppress("unused")
abstract class CommandRequest(val handler: Command, val guild: Guild?, val channel: MessageChannel, val user: User, var command: String, var rawParams: Array<out Any>) {

    abstract val prefix: String
    var params: Array<CommandParam<out Any?>> = emptyArray()

    val replied: Boolean
        get() = reply != null

    var reply: Message? = null

    val paramsStr: String
        get() = params.joinToString("`, `", "`", "`")

    open val forceReplyOnEmpty = false

    override fun toString(): String {
        return "$prefix$command ${params.toList().stream().map { "\"${it.get()?.toString()}\"" }?.collect(Collectors.joining(" "))}"
    }

    fun hasParams(): Boolean {
        return params.isNotEmpty()
    }

    fun hasParam(index: Int): Boolean {
        return try {
            val param = params[index]
            param.get() != null
        } catch (e: IndexOutOfBoundsException) {
            false
        }
    }

    fun joinParams(startIndex: Int, endIndex: Int = params.size): String? {
        if (startIndex > endIndex) return null
        return try {
            params
                .copyOfRange(startIndex, endIndex)
                .toList()
                .joinToString(" ") { it.get()?.toString() ?: "" }
        } catch (e: IndexOutOfBoundsException) {
            null
        }
    }

    fun getParamString(index: Int): String? {
        return (params[index] as StringCommandParam).get()
    }

    fun getParamInt(index: Int): Long? {
        return (params[index] as IntCommandParam).get()
    }

    fun getParamNumber(index: Int): Double? {
        return (params[index] as NumberCommandParam).get()
    }

    fun getParamRole(index: Int): Role? {
        return (params[index] as RoleCommandParam).get()
    }

    fun getParamChannel(index: Int): MessageChannel? {
        return (params[index] as ChannelCommandParam).get()
    }

    fun getParamUser(index: Int): User? {
        return (params[index] as UserCommandParam).get()
    }

    fun getParamMember(index: Int, guild: Guild): Member? {
        return (params[index] as UserCommandParam).asMember(guild)
    }

    abstract fun forceReply(str: String)
    abstract fun forceReply(str: Message)
    abstract fun forceReply(embed: MessageEmbed)
    open fun forceAddReaction(emote: Emote) {}
    open fun forceAddReaction(emote: String) {}
}
