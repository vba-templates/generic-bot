package com.bancarelvalentin.ezbot.process.command.handlers

import com.bancarelvalentin.ezbot.config.ConfigHandler
import com.bancarelvalentin.ezbot.exception.DiscordErrorLogger
import com.bancarelvalentin.ezbot.exception.implerrors.commands.CommandLogicException
import com.bancarelvalentin.ezbot.exception.usererrors.GrantException
import com.bancarelvalentin.ezbot.i18n.DefaultI18n
import com.bancarelvalentin.ezbot.i18n.Localizator
import com.bancarelvalentin.ezbot.logger.LogCategoriesEnum
import com.bancarelvalentin.ezbot.logger.LogCategory
import com.bancarelvalentin.ezbot.logger.Logger
import com.bancarelvalentin.ezbot.process.command.Command
import com.bancarelvalentin.ezbot.process.command.callback.TextCommandCallback
import com.bancarelvalentin.ezbot.process.command.param.CommandParam
import com.bancarelvalentin.ezbot.process.command.request.CommandRequest
import com.bancarelvalentin.ezbot.process.command.response.CommandResponse
import com.bancarelvalentin.ezbot.utils.SentryUtils
import io.sentry.Sentry
import net.dv8tion.jda.api.JDA
import net.dv8tion.jda.api.entities.Guild
import net.dv8tion.jda.api.entities.MessageChannel
import net.dv8tion.jda.api.entities.TextChannel
import net.dv8tion.jda.api.entities.User

abstract class AbstractCommandHandler(val command: Command) {

    protected val gateway: JDA = command.gateway
    protected val logger: Logger = command.logger
    abstract val logCat: LogCategory

    abstract fun getGuild(): Guild
    abstract fun getReplyChannel(): MessageChannel
    abstract fun getUser(): User
    abstract fun parseParams(request: CommandRequest): Array<CommandParam<out Any?>>
    abstract fun getRequest(): CommandRequest?

    fun handle() {
        val request = getRequest()
        if (request != null) {
            val transaction = SentryUtils.createPerf("[COMMAND] " + command.patterns[0], ConfigHandler.config.timeCommandExecutionsInSentry, request.user)
            SentryUtils.configureScope {
                it.setExtra("guildId", request.guild?.id ?: "")
                it.setExtra("guildName", request.guild?.name ?: "")
                it.setExtra("channelId", request.channel.id)
                it.setExtra("channelName", request.channel.name)
            }

            logger.debug("Incoming command ${this.javaClass.simpleName}", logCat)
            val response = CommandResponse(this.command)

            val unauthReason = checkRestrictions(getGuild(), getReplyChannel(), getUser())
            try {
                if (unauthReason == null) {
                    request.params = parseParams(request)
                    Sentry.configureScope {
                        for ((i, p) in request.params.withIndex()) {
                            it.setExtra("param #$i", p.toString())
                        }
                    }

                    logger.trace("Params: ${request.paramsStr}", LogCategoriesEnum.SLASH_COMMAND)
                    this.command.logic.accept(request, response)
                    callBack(command.successCallback.getDeclaredConstructor(CommandRequest::class.java, CommandResponse::class.java).newInstance(request, response))
                    this.command.postLogic?.accept(request, response)
                } else {
                    throw GrantException(unauthReason)
                }
            } catch (e: Exception) {
                response.exception = e
                callBack(
                    command.errorCallback.getDeclaredConstructor(CommandRequest::class.java, CommandResponse::class.java, Exception::class.java).newInstance(request, response, e)
                )
                SentryUtils.addException(transaction, e)
                DiscordErrorLogger.handle(CommandLogicException(command, e))
            } finally {
                transaction?.finish()
            }
        }
    }

    open fun callBack(callback: TextCommandCallback) {
        val embedBuilder = callback.call()
        if (embedBuilder != null) {
            callback.request.forceReply(embedBuilder.build())
        }
    }

    private fun checkRestrictions(guild: Guild?, channel: MessageChannel, author: User): String? {
        var reason: String? = null

        // If command is Guild restricted. We must come from a guild (no DM) AND in the authorized guild
        if (this.command.whitelistGuildIds.isNotEmpty() && (guild == null || !this.command.whitelistGuildIds.contains(guild.idLong))) {
            reason = Localizator.get(DefaultI18n.COMMAND_UNAUTHORIZED_GUILD)
        }

        if (reason == null && channel is TextChannel && this.command.whitelistCategoriesIds.isNotEmpty() && (channel.parentCategory == null || !this.command.whitelistCategoriesIds.contains(channel.parentCategory!!.idLong))) {
            reason = Localizator.get(DefaultI18n.COMMAND_UNAUTHORIZED_CHANNEL_CATEGORY)
        }

        if (reason == null && this.command.whitelistChannelIds.isNotEmpty() && !this.command.whitelistChannelIds.contains(channel.idLong)) {
            reason = Localizator.get(DefaultI18n.COMMAND_UNAUTHORIZED_CHANNEL)
        }

        if (reason == null && (this.command.whitelistUserIds.isNotEmpty() || this.command.whitelistRoleIds.isNotEmpty())) {
            reason = Localizator.get(DefaultI18n.COMMAND_UNAUTHORIZED_USER_OR_ROLE)
            if (this.command.whitelistUserIds.contains(author.idLong)) {
                reason = null
            } else {
                val member = guild?.retrieveMember(author)?.complete()
                if (member == null) {
                    reason += " - " + Localizator.get(DefaultI18n.COMMAND_UNAUTHORIZED_FAILED_TO_LOAD_USER)
                } else {
                    for (role in member.roles) {
                        if (this.command.whitelistRoleIds.contains(role.idLong)) {
                            reason = null
                            break
                        }
                    }
                }
            }
        }
        if (reason != null) logger.debug("Unauthorized: $reason", LogCategoriesEnum.GENERIC_COMMAND)
        return reason
    }
}
