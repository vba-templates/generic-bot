package com.bancarelvalentin.ezbot.process.command.enums

@Suppress("ClassName")
enum class STATUS {
    SUCCESS,
    WARNING,
    ERROR
}
