package com.bancarelvalentin.ezbot.process.command

import com.bancarelvalentin.ezbot.config.ConfigHandler
import com.bancarelvalentin.ezbot.exception.implerrors.commands.CannotInstantiateParamException
import com.bancarelvalentin.ezbot.i18n.DefaultI18n
import com.bancarelvalentin.ezbot.i18n.Localizator
import com.bancarelvalentin.ezbot.process.Instance
import com.bancarelvalentin.ezbot.process.command.callback.TextCommandErrorCallback
import com.bancarelvalentin.ezbot.process.command.callback.TextCommandSuccessCallback
import com.bancarelvalentin.ezbot.process.command.param.ChannelCommandParam
import com.bancarelvalentin.ezbot.process.command.param.CommandParam
import com.bancarelvalentin.ezbot.process.command.param.RoleCommandParam
import com.bancarelvalentin.ezbot.process.command.param.StringCommandParam
import com.bancarelvalentin.ezbot.process.command.param.UserCommandParam
import com.bancarelvalentin.ezbot.process.command.request.CommandRequest
import com.bancarelvalentin.ezbot.process.command.response.CommandResponse
import com.bancarelvalentin.ezbot.process.doc.CommandDoc
import com.bancarelvalentin.ezbot.utils.FormatingUtils
import net.dv8tion.jda.api.EmbedBuilder
import java.util.function.BiConsumer
import java.util.stream.Collectors

@Suppress("SameReturnValue")
abstract class Command : CommandDoc, Instance {

    /**
     * List of guilds on which the command is executable. Empty for any.
     */
    open val whitelistGuildIds: Array<Long> = ConfigHandler.config.defaultWhitelistedGuildIds

    /**
     * List of categories in which the command is executable. Empty for any.
     */
    open val whitelistCategoriesIds: Array<Long> = ConfigHandler.config.defaultWhitelistedCategoriesIds

    /**
     * List of channels in which the command is executable. Empty for any.
     */
    open val whitelistChannelIds: Array<Long> = ConfigHandler.config.defaultWhitelistedChannelIds

    /**
     * List of user ids that can execute the command. Used in combination of whitelistRoleIds. Empty for any.
     */
    open val whitelistUserIds: Array<Long> = ConfigHandler.config.defaultWhitelistedUserIds

    /**
     * List of role ids that can execute the command. Used in combination of whitelistUserIds. Empty for any.
     */
    open val whitelistRoleIds: Array<Long> = ConfigHandler.config.defaultWhitelistedRoleIds

    /**
     * Has this command any channel whitelist
     */
    open val channelPermissionOverrided: Boolean
        get() = whitelistChannelIds.isNotEmpty()

    /**
     * Has this command any role or user whitelist
     */
    open val userPermissionOverrided: Boolean
        get() = whitelistRoleIds.isNotEmpty() || whitelistUserIds.isNotEmpty()

    /**
     * The logic for the command
     */
    abstract val logic: BiConsumer<CommandRequest, CommandResponse>

    /**
     * The code to execute after the logic (To be able to edit reply; for reaction for example)
     */
    open val postLogic: BiConsumer<CommandRequest, CommandResponse>? = null

    /**
     * List of patterns to apply on the first argument of a command (after the prefix has been trimmed)
     */
    abstract val patterns: Array<String>

    /**
     * List of regex build from the patterns
     */
    open val regexes: List<Regex>
        get() = patterns.map { Regex("^$it$", RegexOption.IGNORE_CASE) }

    /**
     * The callback called if no exception occured during the command.
     */
    open val successCallback: Class<out TextCommandSuccessCallback> = TextCommandSuccessCallback::class.java

    /**
     * The callback called if an exception occured during the command execution.
     */
    open val errorCallback: Class<out TextCommandErrorCallback> = TextCommandErrorCallback::class.java

    /**
     * The array describing allowed parameters in their order
     */
    open val paramsClasses: Array<Class<out CommandParam<out Any?>>> = emptyArray()

    override fun getRawTriggers(): List<String> {
        return patterns.toList()
    }

    fun generateDocWhitelistedChannels(): String {
        return whitelistChannelIds.toList().joinToString(", ") { FormatingUtils.formatChannelId(it) }
    }

    fun generateDocWhitelistedRoles(): String {
        return whitelistRoleIds.toList().joinToString(", ") { FormatingUtils.formatRoleId(it) }
    }

    fun generateDocWhitelistedUsers(): String {
        return whitelistUserIds.toList().joinToString(", ") { FormatingUtils.formatUserId(it) }
    }

    fun generateDocWhitelistedRoleAndUsers(): String {
        val roles = generateDocWhitelistedRoles()
        val users = generateDocWhitelistedUsers()
        return roles + if (users.isNotBlank()) ", $users" else ""
    }

    override fun generateDocDesc(): String {
        return rawDesc +
                (if (paramsClasses.isEmpty()) "" else "\n${Localizator.get(DefaultI18n.COMMAND_DOC_SYNTAXE)}: ${getSyntax()}") +
                ("\n${Localizator.get(DefaultI18n.COMMAND_DOC_EXAMPLE)}: `${getSyntaxExample()} $sample`") +
                (if (!channelPermissionOverrided) "" else "\n${Localizator.get(DefaultI18n.COMMAND_DOC_CHANNELS)}: ${generateDocWhitelistedChannels()}") +
                (if (!userPermissionOverrided) "" else "\n${Localizator.get(DefaultI18n.COMMAND_DOC_GRANTS)}: ${generateDocWhitelistedRoleAndUsers()}")
    }

    override fun generateDocDescShort(): String {
        return rawDesc
    }

    private fun getSyntax(): String {
        val paramsStrs = paramsClasses.toList().stream()
            .map {
                try {
                    it.getDeclaredConstructor().newInstance()
                } catch (e: Exception) {
                    throw CannotInstantiateParamException(it, e)
                }
            }
            .map {
                val prefix = when (it) {
                    is StringCommandParam -> "\""
                    is ChannelCommandParam -> "#"
                    is RoleCommandParam -> "@"
                    is UserCommandParam -> "@"
                    else -> ""
                }
                val suffix = when (it) {
                    is StringCommandParam -> "\""
                    else -> ""
                }
                val s = "$prefix${it.rawName}$suffix"
                (if (it.optional) "<" else "[") + s + (if (it.optional) ">" else "]")
            }
            .collect(Collectors.toList())
        return "`${getSyntaxExample()} ${paramsStrs.joinToString()}`"
    }

    @Suppress("LeakingThis")
    fun getSyntaxExample(): String {
        return "${ConfigHandler.config.prefixes[0]}${patterns[0]}"
    }

    override fun getDocEmbedBuilder(): EmbedBuilder {
        val docEmbedBuilder = super.getDocEmbedBuilder()
        getDocMap().forEach {
            docEmbedBuilder.addField(it.first, it.second, false)
        }
        return docEmbedBuilder
    }

    fun getDocMap(): ArrayList<Pair<String, String?>> {
        val list = ArrayList<Pair<String, String?>>()
        for (paramClass in paramsClasses) {
            val param = paramClass.getDeclaredConstructor().newInstance()
            list.add(param.toEmbedField())
        }
        return list
    }
}
