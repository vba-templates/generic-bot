package com.bancarelvalentin.ezbot.process.command.param

import com.bancarelvalentin.ezbot.i18n.DefaultI18n
import com.bancarelvalentin.ezbot.i18n.Localizator
import net.dv8tion.jda.api.JDA
import net.dv8tion.jda.api.interactions.commands.OptionType
import javax.script.ScriptEngine
import javax.script.ScriptEngineManager

abstract class IntCommandParam : CommandParam<Long>(), SupportAutocomplete {

    override val type = OptionType.INTEGER
    override val docType = Localizator.get(DefaultI18n.COMMAND_DOC_PARAM_TYPE_INTEGER)

    override fun setRaw(value: String?, gateway: JDA) {
        try {
            this.value = value?.toLong()
        } catch (numberFormatException: NumberFormatException) {
            val mgr = ScriptEngineManager()
            val engine: ScriptEngine = mgr.getEngineByName("JavaScript")
            try { // Try to evaluate as math expression otherwise rethrow original error
                this.value = engine.eval(value).toString().toLong()
            } catch (e: Exception) {
                throw numberFormatException
            }
        }
    }
}
