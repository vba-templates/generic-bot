package com.bancarelvalentin.ezbot.process.command.param

import com.bancarelvalentin.ezbot.i18n.DefaultI18n
import com.bancarelvalentin.ezbot.i18n.Localizator
import net.dv8tion.jda.api.JDA
import net.dv8tion.jda.api.entities.Role
import net.dv8tion.jda.api.interactions.commands.OptionType

abstract class RoleCommandParam : CommandParam<Role>() {

    override val type = OptionType.ROLE
    override val docType = Localizator.get(DefaultI18n.COMMAND_DOC_PARAM_TYPE_ROLE)

    override fun stringify(): String {
        return get()?.asMention?.replace(">", "/${get()!!.name}>") ?: "null"
    }

    override fun setRaw(value: String?, gateway: JDA) {
        val id = value?.substring(3, value.length - 1)
        this.value = if (id != null) gateway.getRoleById(id) else null
    }
}
