package com.bancarelvalentin.ezbot.process.command.response

import com.bancarelvalentin.ezbot.config.ConfigHandler
import com.bancarelvalentin.ezbot.process.command.Command
import com.bancarelvalentin.ezbot.process.command.enums.STATUS
import com.bancarelvalentin.ezbot.utils.Constants
import net.dv8tion.jda.api.EmbedBuilder
import java.awt.Color

class CommandResponse(@Suppress("unused") val handler: Command) {

    var responseStatus: STATUS = STATUS.SUCCESS

    var title: String? = null
    var defaultTitle: String = when (responseStatus) {
        STATUS.SUCCESS -> "Succés"
        STATUS.WARNING -> "Attention"
        STATUS.ERROR -> "Erreur"
    }
    var message: String? = null
    var embedOverride: EmbedBuilder? = null
    var exception: Exception? = null

    val color: Color
        get() {
            return when (responseStatus) {
                STATUS.SUCCESS -> ConfigHandler.config.defaultColor
                STATUS.WARNING -> Constants.COLOR_WARNING
                STATUS.ERROR -> Constants.COLOR_ERROR
            }
        }

    val emote: String
        get() {
            return when (responseStatus) {
                STATUS.SUCCESS -> Constants.EMOTE_SUCCESS.tounicode()
                STATUS.WARNING -> Constants.EMOTE_WARNING.tounicode()
                STATUS.ERROR -> Constants.EMOTE_ERROR.tounicode()
            }
        }

    override fun toString(): String {
        return if (embedOverride != null) "Embed response"
        else if (title != null || message != null) "${title?.toUpperCase()} | $message"
        else "EMPTY"
    }
}
