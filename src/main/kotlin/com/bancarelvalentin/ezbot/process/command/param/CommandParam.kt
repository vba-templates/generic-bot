package com.bancarelvalentin.ezbot.process.command.param

import com.bancarelvalentin.ezbot.process.doc.CommandParamDoc
import net.dv8tion.jda.api.JDA
import net.dv8tion.jda.api.interactions.commands.OptionType

abstract class CommandParam<T> : CommandParamDoc {

    open val optional: Boolean = false
    protected var value: T? = null
    abstract val type: OptionType
    abstract val docType: String

    abstract fun setRaw(value: String?, gateway: JDA)

    open fun stringify(): String {
        return value?.toString() ?: "null"
    }

    final override fun toString(): String {
        return stringify()
    }

    open fun set(value: T?) {
        this.value = value
    }

    fun get(): T? {
        return value
    }

    override fun generateDocName(): String {
        return super.generateDocName() + " : $docType${if (!optional) "*" else ""}"
    }
}
