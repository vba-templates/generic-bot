package com.bancarelvalentin.ezbot.process.command.param

import com.bancarelvalentin.ezbot.i18n.DefaultI18n
import com.bancarelvalentin.ezbot.i18n.Localizator

open class ExtraParam : StringCommandParam() {

    override val optional = true
    override val rawName = Localizator.get(DefaultI18n.COMMAND_DOC_EXTRA_PARAM_NAME)
    override val rawDesc = Localizator.get(DefaultI18n.COMMAND_DOC_EXTRA_PARAM_DESC)
}
