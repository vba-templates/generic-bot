package com.bancarelvalentin.ezbot.process.scheduled

import com.bancarelvalentin.ezbot.config.ConfigHandler
import com.bancarelvalentin.ezbot.exception.DiscordErrorLogger
import com.bancarelvalentin.ezbot.exception.implerrors.schedules.CronLogicException
import com.bancarelvalentin.ezbot.exception.implerrors.schedules.JobNotFound
import com.bancarelvalentin.ezbot.logger.LogCategoriesEnum
import com.bancarelvalentin.ezbot.process.Process
import com.bancarelvalentin.ezbot.process.doc.ScheduledProcessDoc
import com.bancarelvalentin.ezbot.utils.SentryUtils
import com.coreoz.wisp.Scheduler
import com.coreoz.wisp.schedule.cron.CronSchedule

abstract class ScheduledProcess : ScheduledProcessDoc, Process() {

    // seconds minutes hours day-of-month month day-of-week year
    abstract val cron: String
    val scheduler: Scheduler = Scheduler()

    @Suppress("LeakingThis")
    val jobId: String = uuid.toString()

    @Suppress("unused")
    val started: Boolean
        get() = scheduler.findJob(jobId).isPresent

    abstract fun logic()

    fun registerCron() {
        logger.info("[CRON REGISTERED] ${this.javaClass.simpleName}", LogCategoriesEnum.INITIAL_STARTUP)
        try {
            scheduler.schedule(jobId, {
                val transaction = SentryUtils.createPerf("[CRON] " + this.rawName, ConfigHandler.config.timeCronExecutionsInSentry, gateway.selfUser)
                logger.debug("[CRON-JOB START] ${this.javaClass.simpleName}", LogCategoriesEnum.CRON_JOB)
                try {
                    logic()
                    logger.debug("[CRON-JOB END] ${this.javaClass.simpleName}", LogCategoriesEnum.CRON_JOB)
                } catch (e: Exception) {
                    SentryUtils.addException(transaction, e)
                    DiscordErrorLogger.handle(CronLogicException(this, e))
                    logger.error("[CRON-JOB FATAL ERROR]", LogCategoriesEnum.CRON_JOB, e)
                } finally {
                    transaction?.finish()
                }
            }, CronSchedule.parseQuartzCron(cron))
        } catch (e: Exception) {
            logger.error("[CRON-JOB START ERROR]", LogCategoriesEnum.CRON_JOB, e)
        }
    }

    fun cancel() {
        logger.info("[CRON CANCELED] ${this.javaClass.simpleName}", LogCategoriesEnum.CRON)
        scheduler.cancel(jobId) ?: throw JobNotFound()
    }
}
    