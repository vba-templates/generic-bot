package com.bancarelvalentin.ezbot.default.features.dj

import com.bancarelvalentin.ezbot.process.addreaction.AddReactionParams
import com.bancarelvalentin.ezbot.utils.EmojiEnum
import net.dv8tion.jda.api.entities.User
import net.dv8tion.jda.api.events.message.react.MessageReactionAddEvent

class DjEmoteAction(val emote: EmojiEnum, val desc: String, val logic: (User, AddReactionParams?, MessageReactionAddEvent) -> Unit)
