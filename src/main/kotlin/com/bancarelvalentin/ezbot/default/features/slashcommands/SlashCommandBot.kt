package com.bancarelvalentin.ezbot.default.features.slashcommands

import com.bancarelvalentin.ezbot.i18n.DefaultI18n
import com.bancarelvalentin.ezbot.i18n.Localizator
import com.bancarelvalentin.ezbot.process.EventListener
import com.bancarelvalentin.ezbot.process.background.BackgroundProcess

class SlashCommandBot : BackgroundProcess() {

    override val rawName: String = Localizator.get(DefaultI18n.DEFAULT_PROCESS_DOC_SLASHCOMMANDHANDLER_NAME)
    override val rawDesc: String = Localizator.get(DefaultI18n.DEFAULT_PROCESS_DOC_SLASHCOMMANDHANDLER_DESC)

    override fun getListeners(): Array<EventListener> = arrayOf(SlashCommandListener(this))
}
