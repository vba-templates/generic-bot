package com.bancarelvalentin.ezbot.default.features.chatcommands

import com.bancarelvalentin.ezbot.i18n.DefaultI18n
import com.bancarelvalentin.ezbot.i18n.Localizator
import com.bancarelvalentin.ezbot.process.EventListener
import com.bancarelvalentin.ezbot.process.background.BackgroundProcess

class ChatCommandBot : BackgroundProcess() {

    override val rawName: String = Localizator.get(DefaultI18n.DEFAULT_PROCESS_DOC_CHATCOMMANDHANDLER_DESC)
    override val rawDesc: String = Localizator.get(DefaultI18n.DEFAULT_PROCESS_DOC_CHATCOMMANDHANDLER_NAME)

    override fun getListeners(): Array<EventListener> = arrayOf(ChatCommandListener(this))
}
