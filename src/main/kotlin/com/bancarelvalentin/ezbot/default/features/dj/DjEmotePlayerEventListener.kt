package com.bancarelvalentin.ezbot.default.features.dj

import com.bancarelvalentin.ezbot.config.ConfigHandler
import com.bancarelvalentin.ezbot.dj.EzPlayer
import com.bancarelvalentin.ezbot.dj.State
import com.bancarelvalentin.ezbot.exception.implerrors.startup.DjChannelNotFound
import com.bancarelvalentin.ezbot.i18n.DefaultI18n
import com.bancarelvalentin.ezbot.i18n.Localizator
import com.bancarelvalentin.ezbot.process.EventListener
import com.bancarelvalentin.ezbot.utils.ChannelUtils
import com.bancarelvalentin.ezbot.utils.DjUtils
import com.bancarelvalentin.ezbot.utils.EmojiEnum
import com.bancarelvalentin.ezbot.utils.FormatingUtils.Companion.limit1024
import com.sedmelluq.discord.lavaplayer.track.AudioTrackInfo
import net.dv8tion.jda.api.EmbedBuilder
import net.dv8tion.jda.api.entities.Message
import net.dv8tion.jda.api.entities.MessageEmbed
import net.dv8tion.jda.api.events.ReadyEvent
import java.util.concurrent.TimeUnit

class DjEmotePlayerEventListener(override val process: DjDedicatedChannelPlayerBot) : EventListener(process) {

    private lateinit var message: Message

    override fun onReady(event: ReadyEvent) {
        val channel = gateway.getTextChannelById(ConfigHandler.config.djDedicatedChannelId!!) ?: throw DjChannelNotFound()
        ChannelUtils.emptyChannel(channel)
        channel.manager.setTopic(getTopic()).queue()
        channel.sendMessageEmbeds(getEmbed()).queueAfter(5, TimeUnit.SECONDS) {
            message = it
            process.djEmoteActions.forEach { action ->
                message.addReaction(action.emote.tounicode()).queue()
                process.addReactionEvent(message, action.emote, null) { user, param, event ->
                    try {
                        action.logic.invoke(user, param, event)
                    } catch (throwable: Throwable) {
                        DjLogger.error(null, throwable)
                    }
                    event.reaction.removeReaction(user).queue()
                }
            }
        }
    }

    private fun getTopic(): String? {
        var topic = Localizator.get(DefaultI18n.DJ_PLAYER_CHANNEL_TOPIC)
        return topic
    }

    private fun getEmbed(): MessageEmbed {
        val stateEmote = when (EzPlayer.state) {
            State.PLAYING -> EmojiEnum.PLAY_BUTTON
            State.STOPPED -> EmojiEnum.RED_SQUARE
            State.PAUSED -> EmojiEnum.PAUSE_BUTTON
        }

        val trackInfo = EzPlayer.playingTrack()?.info
        val actionsList = process.djEmoteActions.joinToString("\n") { "${it.emote.tounicode()} ${it.desc}" }
        val history = EzPlayer.getHistory(5).joinToString("\n") { DjUtils.trackToString(it, true) }
        val playingMore = DjUtils.trackToString(trackInfo, true)
        val queue = EzPlayer.getQueue(10).joinToString("\n") { DjUtils.trackToString(it, true) }
        val playing = stateEmote.tounicode() + " " + title(trackInfo)
        val repeatStatus = if (EzPlayer.isRepeatingAll()) Localizator.get(DefaultI18n.DJ_PLAYER_REPEAT_ALL_LABEL) else if (EzPlayer.isRepeatingOne()) Localizator.get(DefaultI18n.DJ_PLAYER_REPEAT_ONE_LABEL) else null
        val footer = playing + if (repeatStatus != null) " - $repeatStatus" else ""

        return EmbedBuilder()
            .setTitle(playing)
            .addField(Localizator.get(DefaultI18n.DJ_PLAYER_ACTIONS_LABEL), limit1024(actionsList), false)
            .addField(Localizator.get(DefaultI18n.DJ_PLAYER_HISTORY_LABEL), limit1024(history), false)
            .addField(Localizator.get(DefaultI18n.DJ_PLAYER_PLAYING_LABEL), limit1024(playingMore), false)
            .addField(Localizator.get(DefaultI18n.DJ_PLAYER_QUEUE_LABEL), limit1024(queue), false)
            .setFooter(footer)
            .build()
    }

    private fun title(trackInfo: AudioTrackInfo?) = trackInfo?.title ?: Localizator.get(DefaultI18n.DJ_PLAYER_STATE_NO_TRACK_PLAYING)

    fun updatePlaying() {
        message.editMessageEmbeds(getEmbed()).queue()
    }
}
