package com.bancarelvalentin.ezbot.default.features.liveconfig

import com.bancarelvalentin.ezbot.Orchestrator
import com.bancarelvalentin.ezbot.config.ConfigHandler
import com.bancarelvalentin.ezbot.i18n.DefaultI18n
import com.bancarelvalentin.ezbot.i18n.Localizator
import com.bancarelvalentin.ezbot.process.EventListener
import com.bancarelvalentin.ezbot.process.background.BackgroundProcess
import com.bancarelvalentin.ezbot.utils.ChannelUtils
import com.fasterxml.jackson.databind.ObjectMapper
import net.dv8tion.jda.api.entities.TextChannel

class LiveConfigHandlerBot : BackgroundProcess() {
    override val rawName: String = Localizator.get(DefaultI18n.DEFAULT_PROCESS_DOC_LIVECONFIGHANDLER_NAME)
    override val rawDesc: String = Localizator.get(DefaultI18n.DEFAULT_PROCESS_DOC_LIVECONFIGHANDLER_DESC)

    override fun getListeners(): Array<EventListener> = arrayOf(LiveConfigHandlerReadyListener(this), LiveConfigHandlerMessageListener(this))

    companion object {
        lateinit var channel: TextChannel
        fun saveInChat() {
            ChannelUtils.emptyChannel(channel)
            val json = ObjectMapper().writerWithDefaultPrettyPrinter().writeValueAsString(ConfigHandler.liveConfig)
            channel.sendMessage("```json\n$json\n```").queue()
        }

        fun load(json: String) {
            Orchestrator.liveConfigReady()
            ConfigHandler.liveConfig = ObjectMapper().readValue(json, ConfigHandler.liveConfig!!.javaClass)
            if (!ConfigHandler.config.disableLiveConfigEdit) saveInChat()
        }
    }
}
