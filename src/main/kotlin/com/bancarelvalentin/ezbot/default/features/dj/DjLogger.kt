package com.bancarelvalentin.ezbot.default.features.dj

import com.bancarelvalentin.ezbot.config.ConfigHandler
import com.bancarelvalentin.ezbot.config.EnvConfig
import com.bancarelvalentin.ezbot.exception.implerrors.startup.DjChannelNotFound
import com.bancarelvalentin.ezbot.exception.usererrors.AbstractUserException
import com.bancarelvalentin.ezbot.i18n.DefaultI18n
import com.bancarelvalentin.ezbot.i18n.Localizator
import com.bancarelvalentin.ezbot.process.EventListener
import com.bancarelvalentin.ezbot.utils.AsyncUtils
import com.bancarelvalentin.ezbot.utils.Constants
import com.bancarelvalentin.ezbot.utils.DjUtils
import com.bancarelvalentin.ezbot.utils.EmojiEnum
import com.bancarelvalentin.ezbot.utils.ExceptionUtils
import com.bancarelvalentin.ezbot.utils.FormatingUtils.Companion.limit1024
import com.sedmelluq.discord.lavaplayer.track.AudioTrack
import com.sedmelluq.discord.lavaplayer.track.AudioTrackInfo
import net.dv8tion.jda.api.EmbedBuilder
import net.dv8tion.jda.api.entities.MessageEmbed
import net.dv8tion.jda.api.entities.TextChannel
import java.time.Duration

class DjLogger(override val process: DjDedicatedChannelPlayerBot) : EventListener(process) {

    companion object {
        var instance: DjLogger? = null
        fun nowPlaying(info: AudioTrackInfo) = instance?.nowPlaying(info)
        fun resumed(info: AudioTrackInfo) = instance?.resumed(info)
        fun paused() = instance?.paused()
        fun stopped() = instance?.stopped()
        fun emptyQueue() = instance?.emptyQueue()
        fun stoppedForinactivity() = instance?.stoppedForinactivity()
        fun stuck(info: AudioTrackInfo) = instance?.stuck(info)
        fun error(info: AudioTrackInfo?, exception: Throwable) = instance?.error(info, exception)
        fun queued(tracks: Array<AudioTrack>) = instance?.queued(tracks)
        fun refreshMeta() = instance?.process?.actionHandler?.updatePlaying()
    }

    init {
        instance = if (ConfigHandler.config.djDedicatedChannelId != null) this else null
    }

    lateinit var channel: TextChannel

    private fun nowPlaying(info: AudioTrackInfo) {
        process.actionHandler.updatePlaying()
        val default = "${Localizator.get(DefaultI18n.DJ_PLAYER_LOG_NOW_PLAYING)} : ${DjUtils.trackToString(info)}"
        val plus = moreInfoEmbed(info)
        log(default, reactions = arrayOf(Pair(EmojiEnum.PLUS, plus)))
    }

    private fun paused() {
        process.actionHandler.updatePlaying()
        val default = Localizator.get(DefaultI18n.DJ_PLAYER_LOG_PAUSED)
        log(default)
    }

    private fun stopped() {
        process.actionHandler.updatePlaying()
        val default = Localizator.get(DefaultI18n.DJ_PLAYER_LOG_STOPPED)
        log(default)
    }

    private fun stoppedForinactivity() {
        process.actionHandler.updatePlaying()
        val default = Localizator.get(DefaultI18n.DJ_PLAYER_LOG_STOPPED_FOR_INACTIVITY)
        log(default)
    }

    private fun emptyQueue() {
        process.actionHandler.updatePlaying()
        val default = Localizator.get(DefaultI18n.DJ_PLAYER_LOG_EMPTYED_QUEUE)
        log(default)
    }

    private fun resumed(info: AudioTrackInfo) {
        process.actionHandler.updatePlaying()
        val default = "${Localizator.get(DefaultI18n.DJ_PLAYER_LOG_RESUMED)} : **${info.title}** - *${info.author}*"
        val plus = moreInfoEmbed(info)
        log(default, reactions = arrayOf(Pair(EmojiEnum.PLUS, plus)))
    }

    private fun queued(tracks: Array<AudioTrack>) {
        process.actionHandler.updatePlaying()
        val tracksStr = tracks.map { track -> track.info }.joinToString("\n") { info -> DjUtils.trackToString(info, true) }
        val title = Localizator.get(DefaultI18n.DJ_PLAYER_LOG_QUEUED)
        log(title, {
            val i18n = Localizator.get(DefaultI18n.DJ_PLAYER_QUEUE_LABEL)
            it.addField(
                limit1024(i18n),
                limit1024(tracksStr),
                false
            )
        })
    }

    private fun error(info: AudioTrackInfo?, exception: Throwable?) {
        val plus = if (exception != null) arrayOf(Pair(EmojiEnum.PLUS, ExceptionUtils.toEmbed(exception, false))) else emptyArray()
        if (exception != null && exception is AbstractUserException)
            log(exception.message, { it.setColor(Constants.COLOR_WARNING) }, *plus)
        else if (info == null)
            log(Localizator.get(DefaultI18n.DJ_PLAYER_LOG_UNKNOWN_ERROR), { it.setColor(Constants.COLOR_ERROR) }, *plus)
        else
            log(Localizator.get(DefaultI18n.DJ_PLAYER_LOG_UNKNOWN_ERROR_ON_TRACK, info.title, info.author, info.identifier, info.uri), { it.setColor(Constants.COLOR_ERROR) }, *plus)
    }

    private fun stuck(info: AudioTrackInfo) {
        error(info, null)
    }

    private fun log(message: String, editEmbed: ((EmbedBuilder) -> Unit)? = null, vararg reactions: Pair<EmojiEnum, MessageEmbed>) {
        if (!this::channel.isInitialized) {
            channel = gateway.getTextChannelById(ConfigHandler.config.djDedicatedChannelId!!) ?: throw DjChannelNotFound()
        }

        val builder = default().setDescription(message)
        editEmbed?.invoke(builder)
        channel.sendMessageEmbeds(builder.build()).queue {
            reactions.forEach { pair ->
                it.addReaction(pair.first.tounicode()).queue()
                process.addReactionEvent(it, pair.first, null) { _, _, _ -> it.replyEmbeds(pair.second).queue() }
            }

            val duration =
                if (EnvConfig.DEV) Duration.ofSeconds(10)
                else Duration.ofMinutes(5)
            AsyncUtils.delay(duration) {
                it.delete().queue()
            }
        }
    }

    private fun moreInfoEmbed(info: AudioTrackInfo) = default()
        .setTitle(info.title)
        .addField(Localizator.get(DefaultI18n.DJ_PLAYER_LOG_NOW_PLAYING_AUTHOR_LABEL), info.author, true)
        .addField(Localizator.get(DefaultI18n.DJ_PLAYER_LOG_NOW_PLAYING_DURATION_LABEL), formatedDuration(info.length), true)
        .setDescription(info.uri)
        .build()

    private fun formatedDuration(durationInMilliseconds: Long): String {
        val durationInSeconds = durationInMilliseconds / 1000
        val formated = String.format("%d:%02d:%02d", durationInSeconds / 3600, (durationInSeconds % 3600) / 60, (durationInSeconds % 60))
        return if (formated.startsWith("0:")) {
            formated.substring(2)
        } else {
            formated
        }
    }

    private fun default(): EmbedBuilder {
        return EmbedBuilder()
            .setColor(ConfigHandler.config.defaultColor)
            .setFooter(ConfigHandler.config.version ?: "")
    }
}
