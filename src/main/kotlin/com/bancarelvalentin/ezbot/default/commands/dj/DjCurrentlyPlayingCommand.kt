package com.bancarelvalentin.ezbot.default.commands.dj

import com.bancarelvalentin.ezbot.dj.EzPlayer
import com.bancarelvalentin.ezbot.i18n.DefaultI18n
import com.bancarelvalentin.ezbot.i18n.Localizator
import com.bancarelvalentin.ezbot.process.command.request.CommandRequest
import com.bancarelvalentin.ezbot.process.command.response.CommandResponse
import java.util.function.BiConsumer

open class DjCurrentlyPlayingCommand : AbstractDjCommand("currently_playing", "cp") {
    override val rawName: String = Localizator.get(DefaultI18n.DEFAULT_PROCESS_DOC_DJ_CURRENTLY_PLAYING_NAME)
    override val rawDesc: String = Localizator.get(DefaultI18n.DEFAULT_PROCESS_DOC_DJ_CURRENTLY_PLAYING_DESC)

    override val djLogic = BiConsumer { _: CommandRequest, response: CommandResponse ->
        val track = EzPlayer.playingTrack()
        response.title = Localizator.get(DefaultI18n.DEFAULT_PROCESS_DOC_DJ_CURRENTLY_PLAYING_NAME)
        response.message = track?.info?.author + " | " + track?.info?.title
    }
}
