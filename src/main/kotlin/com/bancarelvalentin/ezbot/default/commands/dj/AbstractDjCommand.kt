package com.bancarelvalentin.ezbot.default.commands.dj

import com.bancarelvalentin.ezbot.config.ConfigHandler
import com.bancarelvalentin.ezbot.process.command.Command
import com.bancarelvalentin.ezbot.process.command.request.CommandRequest
import com.bancarelvalentin.ezbot.process.command.response.CommandResponse
import java.util.function.BiConsumer

abstract class AbstractDjCommand(val commandKey: String, val alias: String = commandKey[0].toString()) : Command() {

    final override val patterns: Array<String> = arrayOf("dj_$commandKey", "dj$alias")
    final override val whitelistGuildIds = ConfigHandler.config.extraDjWhitelistedGuildIds
    final override val whitelistCategoriesIds = ConfigHandler.config.extraDjWhitelistedCategoriesIds
    final override val whitelistRoleIds = ConfigHandler.config.extraDjWhitelistedRoleIds
    final override val whitelistUserIds = ConfigHandler.config.extraDjWhitelistedUserIds

    abstract val djLogic: BiConsumer<CommandRequest, CommandResponse>

    final override val logic = BiConsumer { request: CommandRequest, response: CommandResponse ->
        djLogic.accept(request, response)
    }
}
