package com.bancarelvalentin.ezbot.default.commands.dj

import com.bancarelvalentin.ezbot.dj.EzPlayer
import com.bancarelvalentin.ezbot.exception.usererrors.player.MustBeInAVoiceChannel
import com.bancarelvalentin.ezbot.i18n.DefaultI18n
import com.bancarelvalentin.ezbot.i18n.Localizator
import com.bancarelvalentin.ezbot.process.command.param.CommandParam
import com.bancarelvalentin.ezbot.process.command.request.CommandRequest
import com.bancarelvalentin.ezbot.process.command.response.CommandResponse
import com.bancarelvalentin.ezbot.utils.DjUtils
import java.util.function.BiConsumer

open class DjQueueCommand : AbstractDjCommand("queue") {
    override val rawName: String = Localizator.get(DefaultI18n.DEFAULT_PROCESS_DOC_DJ_ADD_TO_QUEUE_NAME)
    override val rawDesc: String = Localizator.get(DefaultI18n.DEFAULT_PROCESS_DOC_DJ_ADD_TO_QUEUE_DESC)
    override val paramsClasses: Array<Class<out CommandParam<out Any?>>> = arrayOf(QueryParam::class.java)

    override val djLogic = BiConsumer { request: CommandRequest, response: CommandResponse ->
        request.guild?.retrieveMember(request.user)?.queue { member ->
            val channel = member?.voiceState?.channel ?: throw MustBeInAVoiceChannel()
            val paramString = request.getParamString(0)!!
            var over = false
            var ex: Throwable? = null
            EzPlayer.searchAndQueue(
                paramString, channel,
                { trackInfos ->
                    response.message = trackInfos.joinToString("\n") { DjUtils.trackToString(it, true) }
                    over = true
                },
                { ex = it as Throwable },
            )
            while (!over && ex == null) {
                /* I'm sorry for this */
            }
            if (ex != null) {
                throw ex as Throwable
            }
        }
    }
}
