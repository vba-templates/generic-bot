package com.bancarelvalentin.ezbot.default.commands.help

import com.bancarelvalentin.ezbot.Orchestrator
import com.bancarelvalentin.ezbot.config.ConfigHandler
import com.bancarelvalentin.ezbot.i18n.DefaultI18n
import com.bancarelvalentin.ezbot.i18n.Localizator
import com.bancarelvalentin.ezbot.process.command.Command
import com.bancarelvalentin.ezbot.process.command.enums.STATUS
import com.bancarelvalentin.ezbot.process.command.param.CommandParam
import com.bancarelvalentin.ezbot.process.command.request.CommandRequest
import com.bancarelvalentin.ezbot.process.command.response.CommandResponse
import net.dv8tion.jda.api.EmbedBuilder
import java.util.function.BiConsumer

class HelpTextCommand : Command() {

    override val patterns = arrayOf("help", "h", "aide")
    override val rawName: String = Localizator.get(DefaultI18n.DEFAULT_PROCESS_DOC_HELP_NAME)
    override val rawDesc: String = Localizator.get(DefaultI18n.DEFAULT_PROCESS_DOC_HELP_DESC)
    override val sample = "help status"
    override val paramsClasses: Array<Class<out CommandParam<out Any?>>> = arrayOf(TargetParam::class.java)

    override val logic = BiConsumer { request: CommandRequest, response: CommandResponse ->
        var commandTarget: Command? = null
        val target = request.getParamString(0)
        if (target != null && request.hasParams()) {
            for (runningProcess: Command in Orchestrator.getCommands()) {
                for (regex in runningProcess.regexes) {
                    if (regex.matches(target)) {
                        commandTarget = runningProcess
                    }
                }
            }
        }
        if (commandTarget == null) {
            response.embedOverride = getEmbed(request.guild?.idLong)
            if (target != null) {
                response.responseStatus = STATUS.WARNING
                response.embedOverride!!.setColor(response.color)
            }
        } else {
            response.embedOverride = commandTarget.getDocEmbedBuilder()
        }
    }

    private fun getEmbed(guildIdFilter: Long?): EmbedBuilder {
        val embedBuilder = EmbedBuilder()
            .setColor(ConfigHandler.config.defaultColor)
            .setTitle("Prefixes: ${ConfigHandler.config.prefixes.toList().joinToString("`, `", "`", "`")}")
            .setDescription(Localizator.get(DefaultI18n.HELP_COMMAND_CONTENT))
            .addBlankField(true)

        for (b: Command in Orchestrator.getCommands()) {
            if (b.whitelistGuildIds.isNotEmpty() && (guildIdFilter == null || !b.whitelistGuildIds.contains(guildIdFilter))) continue // Skip commands not available on this guild
            val field = b.toEmbedField()
            embedBuilder.addField(field.first, field.second, false)
        }
        return embedBuilder
    }
}
