package com.bancarelvalentin.ezbot.utils

import com.bancarelvalentin.ezbot.dj.EzPlayer
import com.sedmelluq.discord.lavaplayer.track.AudioTrackInfo
import net.dv8tion.jda.api.entities.User
import net.dv8tion.jda.api.events.message.react.MessageReactionAddEvent

class DjUtils {
    companion object {
        fun queueShortcut(event: MessageReactionAddEvent, user: User, s: String) {
            event.guild.retrieveMember(user).queue {
                EzPlayer.searchAndQueue(s, it.voiceState?.channel)
            }
        }

        fun trackToString(info: AudioTrackInfo?, full: Boolean = false): String {
            return if (info == null) ""
            else "*${info.author}* - **${info.title}**" + (if (full) " (${info.uri})" else "")
        }
    }
}
