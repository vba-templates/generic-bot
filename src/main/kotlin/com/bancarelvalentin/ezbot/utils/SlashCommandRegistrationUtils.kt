package com.bancarelvalentin.ezbot.utils

import com.bancarelvalentin.ezbot.Orchestrator
import com.bancarelvalentin.ezbot.logger.LogCategoriesEnum
import com.bancarelvalentin.ezbot.logger.Logger
import com.bancarelvalentin.ezbot.process.command.Command
import com.bancarelvalentin.ezbot.process.command.param.ChannelCommandParam
import com.bancarelvalentin.ezbot.process.command.param.CommandParam
import com.bancarelvalentin.ezbot.process.command.param.IntCommandParam
import com.bancarelvalentin.ezbot.process.command.param.RoleCommandParam
import com.bancarelvalentin.ezbot.process.command.param.StringCommandParam
import com.bancarelvalentin.ezbot.process.command.param.SupportAutocomplete
import com.bancarelvalentin.ezbot.process.command.param.UserCommandParam
import net.dv8tion.jda.api.JDA
import net.dv8tion.jda.api.events.interaction.SlashCommandEvent
import net.dv8tion.jda.api.interactions.commands.OptionType
import net.dv8tion.jda.api.interactions.commands.build.OptionData
import net.dv8tion.jda.api.requests.restaction.CommandCreateAction

@Suppress("unused")
class SlashCommandRegistrationUtils {

    companion object {

        val logger = Logger(SlashCommandRegistrationUtils::class.java)

        fun register(gateway: JDA, command: Command) {
            val commandStr = formatSlashCommandDocTitle(command.patterns[0])
            val foormatedDesc = formatSlashCommandDocDescritpion(command.rawDesc)

            // Creating command in correct scope
            val commands = if (command.whitelistGuildIds.isNotEmpty()) {
                logger.trace("Registering command ${command.patterns[0]} in some guilds only.", LogCategoriesEnum.INITIAL_STARTUP)
                command.whitelistGuildIds.map {
                    val guild = Orchestrator.gateway.getGuildById(it) ?: throw RuntimeException("Guild $it not found")
                    val insertedCommand = guild.upsertCommand(commandStr, foormatedDesc)
                    logger.trace("Added to guild ${guild.name}.", LogCategoriesEnum.INITIAL_STARTUP)
                    insertedCommand
                }.toTypedArray()
            } else {
                logger.trace("Registering command ${command.patterns[0]} publicly.", LogCategoriesEnum.INITIAL_STARTUP)
                arrayOf(gateway.upsertCommand(commandStr, foormatedDesc))
            }

            // Ading params
            for (instance in command.paramsClasses.map { it.getDeclaredConstructor().newInstance() }.sortedBy { it.optional }) {
                logger.trace("Adding param ${instance.rawName} (optional: ${instance.optional})", LogCategoriesEnum.INITIAL_STARTUP)
                commands.forEach { addOption(it, instance) }
            }

            // Registering
            commands.forEach {
                try {
                    it.queue()
                } catch (e: Exception) {
                    logger.error("Couldn't register command ${command.patterns[0]}. Your bot need the oauth2 scope 'applications.commands' to register commands. See https://stackoverflow.com/a/69341316/3376635", LogCategoriesEnum.INITIAL_STARTUP, e)
                }
            }

            logger.info("Queued slash command(s) $commandStr for registration", LogCategoriesEnum.INITIAL_STARTUP)
        }

        private fun addOption(command: CommandCreateAction, instance: CommandParam<out Any?>) {
            val option = when (instance) {
                is RoleCommandParam -> OptionType.ROLE
                is UserCommandParam -> OptionType.USER
                is ChannelCommandParam -> OptionType.CHANNEL
                is StringCommandParam -> OptionType.STRING
                is IntCommandParam -> OptionType.INTEGER
                else -> OptionType.STRING
            }
            val optionData = OptionData(option, formatSlashCommandDocTitle(instance.rawName), formatSlashCommandDocDescritpion(instance.rawDesc), !instance.optional)
            if (instance is SupportAutocomplete) {
                optionData.addChoices(instance.choices)
            }
            command.addOptions(optionData)
        }

        fun formatSlashCommandDocTitle(title: String): String {
            return formatSlashCommandDoc(title, true)
        }

        fun formatSlashCommandDocDescritpion(description: String): String {
            return formatSlashCommandDoc(description, false)
        }

        /**
         * Clean the command name or desc according to discord requirements
         */
        private fun formatSlashCommandDoc(name: String, isCommandName: Boolean = false): String {
            var result = name.ifBlank { "-" }
            val limit = if (isCommandName) 32 else 100
            if (result.length > limit) {
                result = result.substring(0, limit - 3)
                result += "..."
            }
            if (isCommandName) result = result.replace(Regex("([^\\w+]|\\s+)"), "-").toLowerCase()
            return result
        }

        private fun toString(event: SlashCommandEvent): String {
            return "${event.name} | ${event.options.joinToString("\t-\t") { it.name + "(${it.type} - ${it.asString})" }}"
        }
    }
}
