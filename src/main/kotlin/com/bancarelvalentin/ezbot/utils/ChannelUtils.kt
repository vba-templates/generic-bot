package com.bancarelvalentin.ezbot.utils

import net.dv8tion.jda.api.entities.Guild
import net.dv8tion.jda.api.entities.GuildChannel
import net.dv8tion.jda.api.entities.Message
import net.dv8tion.jda.api.entities.MessageChannel
import net.dv8tion.jda.api.entities.TextChannel
import net.dv8tion.jda.api.entities.VoiceChannel
import java.util.stream.Stream

@Suppress("unused")
class ChannelUtils {
    companion object {
        fun getChannelsHavingRoleOverride(
            guilds: Array<Guild>,
            roles: Array<Long>,
            bits: Array<Long>? = null,
            categoriesId: Array<Long>? = null,
            text: Boolean = true,
            voice: Boolean = true,
        ): Stream<GuildChannel> {
            return guilds.toList().stream().flatMap {
                it.channels.stream()
                    .filter { channel -> (text && channel is TextChannel) || (voice && channel is VoiceChannel) }
                    .map { ch -> if (text) it.getTextChannelById(ch.id) else it.getVoiceChannelById(ch.id) }
                    .filter { channel -> channel != null && (categoriesId == null || (channel.parentCategory != null && categoriesId.contains(channel.parentCategory!!.idLong))) }
                    .filter { channel ->
                        channel!!.permissionOverrides.stream().anyMatch { override ->
                            val roleOk = roles.contains(override.role?.idLong) // And concern the role passed in param
                            val bitOk =
                                bits == null || bits.contains(override.allowedRaw) // Must have only permissions set with this bit
                            roleOk && bitOk
                        }
                    }
            }
        }

        fun emptyChannel(channel: MessageChannel) {
            val history = channel.history
            var msgs = history.retrievePast(100).complete()
            while (msgs.isNotEmpty()) {
                channel.purgeMessages(msgs)
                msgs = history.retrievePast(100).complete()
            }
        }

        fun forEachMessageInChannel(channel: MessageChannel, logic: (Message) -> Unit) {
            var messageListRaw: List<Message> = channel.getHistoryFromBeginning(100).complete().retrievedHistory
            messageListRaw.forEach { logic.invoke(it) }

            var over = messageListRaw.size < 100
            while (!over) {
                messageListRaw = channel.getHistoryAfter(messageListRaw.last(), 100).complete().retrievedHistory
                messageListRaw.forEach { logic.invoke(it) }
                over = messageListRaw.size < 100
            }
        }
    }
}
