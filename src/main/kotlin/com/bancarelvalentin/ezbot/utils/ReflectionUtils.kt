package com.bancarelvalentin.ezbot.utils

import org.reflections.Reflections
import java.lang.reflect.Modifier
import java.util.stream.Stream

class ReflectionUtils {
    companion object {
        inline fun <reified T : Any> getAllClassesOfType(): Stream<Class<out T>> {
            return Thread.currentThread().contextClassLoader.definedPackages.toList().stream()
                .flatMap { pckage ->
                    val reflections = Reflections(pckage.name)
                    val classes: Set<Class<out T>> = reflections.getSubTypesOf(T::class.java)
                    classes.stream()
                        .filter { clazz -> !clazz.isInterface && clazz.declaredConstructors.isNotEmpty() && !Modifier.isAbstract(clazz.modifiers) }
                }
        }
    }
}
