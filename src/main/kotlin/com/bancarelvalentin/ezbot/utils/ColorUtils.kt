package com.bancarelvalentin.ezbot.utils

import java.awt.Color
import java.io.BufferedInputStream
import java.util.Collections
import java.util.LinkedList
import javax.imageio.ImageIO
import javax.imageio.ImageReader

@Suppress("unused")
class ColorUtils {
    companion object {

        fun hexToColor(hex: String): Color {
            return Color(
                Integer.valueOf(hex.substring(1, 3), 16),
                Integer.valueOf(hex.substring(3, 5), 16),
                Integer.valueOf(hex.substring(5, 7), 16)
            )
        }

        fun colorToHex(color: Color, capital: Boolean = true): String {
            val x = if (capital) "X" else "x"
            return String.format("#%02$x%02$x%02$x", color.red, color.green, color.blue)
        }

        /**
         * https://stackoverflow.com/a/10530562/3376635
         */
        fun getDominant(file: BufferedInputStream): Color {
            return try {
                val stream = ImageIO.createImageInputStream(file)
                val iter: Iterator<*> = ImageIO.getImageReaders(stream)
                if (!iter.hasNext()) {
                    throw RuntimeException("Cannot load the specified file $file")
                }
                val imageReader = iter.next() as ImageReader
                imageReader.input = stream
                val image = imageReader.read(0)
                val height = image.height
                val width = image.width
                val m: MutableMap<Int, Int> = HashMap()
                for (i in 0 until width) {
                    for (j in 0 until height) {
                        val rgb = image.getRGB(i, j)
                        val rgbArr = getRGBArr(rgb)
                        // Filter out grays....
                        if (!isGray(rgbArr)) {
                            var counter = m[rgb]
                            if (counter == null) counter = 0
                            counter++
                            m[rgb] = counter
                        }
                    }
                }
                getMostCommonColour(m)
            } catch (e: RuntimeException) {
                Color.BLACK
            }
        }

        @Suppress("UNCHECKED_CAST")
        private fun getMostCommonColour(map: MutableMap<Int, Int>): Color {
            val list: List<*> = LinkedList<Any?>(map.entries)
            Collections.sort<Any>(list) { o1, o2 ->
                ((o1 as Map.Entry<Int, Int>).value as Comparable<Int>)
                    .compareTo((o2 as Map.Entry<Int, Int>).value)
            }
            val me = list[list.size - 1] as Map.Entry<Int, Int>
            val rgb = getRGBArr((me.key as Int?)!!)
            return Color(rgb[0], rgb[1], rgb[2])
        }

        private fun getRGBArr(pixel: Int): IntArray {
            @Suppress("UNUSED_VARIABLE") val alpha = pixel shr 24 and 0xff
            val red = pixel shr 16 and 0xff
            val green = pixel shr 8 and 0xff
            val blue = pixel and 0xff
            return intArrayOf(red, green, blue)
        }

        private fun isGray(rgbArr: IntArray): Boolean {
            val rgDiff = rgbArr[0] - rgbArr[1]
            val rbDiff = rgbArr[0] - rgbArr[2]
            // Filter out black, white and grays...... (tolerance within 10 pixels)
            val tolerance = 10
            if (rgDiff > tolerance || rgDiff < -tolerance) if (rbDiff > tolerance || rbDiff < -tolerance) {
                return false
            }
            return true
        }
    }
}
