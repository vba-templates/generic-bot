package com.bancarelvalentin.ezbot.utils

import com.bancarelvalentin.ezbot.config.ConfigHandler
import com.bancarelvalentin.ezbot.utils.FormatingUtils.Companion.limit1024
import net.dv8tion.jda.api.EmbedBuilder
import net.dv8tion.jda.api.entities.MessageEmbed
import java.awt.Color
import java.io.PrintWriter
import java.io.StringWriter
import java.text.SimpleDateFormat
import java.util.Date
import java.util.regex.Matcher
import java.util.regex.Pattern
import java.util.stream.Collectors

@Suppress("unused")
class ExceptionUtils {
    companion object {

        fun stackAsString(exception: Throwable, packageNameCount: Int = -1, vararg filters: String): String {
            val sw = StringWriter()
            val pw = PrintWriter(sw)
            exception.printStackTrace(pw)

            var trace = sw.toString()
            if (packageNameCount >= 0 || filters.isNotEmpty()) {
                trace = trace.split("\n").stream()
                    .map { oneStacktracceLineAsString(it, filters, packageNameCount) }
                    .filter { it != null }
                    .collect(Collectors.joining("\n"))
            }
            return trace
        }

        private fun oneStacktracceLineAsString(line: String, filters: Array<out String>, packageNameCount: Int): String? {
            val passFilter = filters.isEmpty() || filters.any { filter -> line.contains(filter) }
            val oneStackElementPattern = Pattern.compile("\\s*at (.*)\\.(.*)\\.(.*)\\((.*)\\)") // $1=package, $2=class, $3=method, $4=class:line ref
            val matcher = oneStackElementPattern.matcher(line)

            return if (matcher.find()) { // Line is a stacktrace element
                if (passFilter) {
                    oneStacktraceElementLineAsString(packageNameCount, matcher)
                } else {
                    null
                }
            } else { // Line containing the exception message
                if (passFilter) {
                    line.substring(maxOf(line.indexOf(":") + 2, 0))
                } else {
                    line
                }
            }
        }

        private fun oneStacktraceElementLineAsString(packageNameCount: Int, matcher: Matcher): String {
            val classAndLineRef = matcher.group(4)
            val method = matcher.group(3)
            val clazz = matcher.group(2)
            var packagee = matcher.group(1)
            packagee = if (packageNameCount > 0) {
                val packages = packagee.split(".").toList()
                packages.stream().skip((packages.size - packageNameCount).toLong()).collect(Collectors.joining(".")) + "."
            } else if (packageNameCount == 0) {
                ""
            } else {
                packagee
            }
            return "---- at $packagee$clazz.$method($classAndLineRef)"
        }

        fun toEmbed(exception: Throwable, shorten: Boolean = true, color: Color = Constants.COLOR_ERROR): MessageEmbed {
            val message = exception.message ?: "*no message*"
            return EmbedBuilder()
                .setTitle(limit1024(message))
                .addField("Type", exception.javaClass.simpleName, true)
                .setDescription(stackAsString(exception, if (shorten) 0 else -1, *if (shorten) arrayOf("com.bancarelvalentin") else emptyArray()))
                .setColor(color)
                .setFooter((ConfigHandler.config.version ?: "") + " - " + SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S").format(Date()))
                .build()
        }
    }
}
