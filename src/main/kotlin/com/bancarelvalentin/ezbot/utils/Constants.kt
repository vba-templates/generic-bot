package com.bancarelvalentin.ezbot.utils

import java.awt.Color

class Constants {

    @Suppress("unused")
    companion object {
        const val SIMPLE_LOCALIZE_CDN_URL = "https://cdn.simplelocalize.io"
        const val SIMPLE_LOCALIZE_API_URL = "https://api.simplelocalize.io/api/v1"
        const val I18N_PROJECT_TOKEN = "192f87afc8884e88ad0305f3f24be250"

        val COLOR_SUCCESS: Color = Color.GREEN
        val COLOR_WARNING: Color = Color.ORANGE
        val COLOR_ERROR: Color = Color.RED

        val EMOTE_SUCCESS = EmojiEnum.CHECK_MARK_BUTTON
        val EMOTE_WARNING = EmojiEnum.WARNING
        val EMOTE_ERROR = EmojiEnum.CROSS_MARK
    }
}
