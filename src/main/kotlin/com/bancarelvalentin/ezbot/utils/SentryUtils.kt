package com.bancarelvalentin.ezbot.utils

import com.bancarelvalentin.ezbot.config.ConfigHandler
import com.bancarelvalentin.ezbot.exception.implerrors.AbstractImplementationException
import com.bancarelvalentin.ezbot.exception.implerrors.startup.MissingSentryDSN
import com.bancarelvalentin.ezbot.exception.usererrors.AbstractUserException
import com.bancarelvalentin.ezbot.exception.usererrors.GrantException
import io.sentry.ITransaction
import io.sentry.ScopeCallback
import io.sentry.Sentry
import io.sentry.SentryLevel
import io.sentry.SpanStatus
import net.dv8tion.jda.api.entities.User

class SentryUtils {
    companion object {

        private fun toSentryUser(user: User): io.sentry.protocol.User {
            return io.sentry.protocol.User().apply {
                id = user.id
                username = user.name
            }
        }

        private fun exceptionToSentryLevel(throwable: Throwable) = when (throwable) {
            is GrantException -> SentryLevel.DEBUG
            is AbstractUserException -> SentryLevel.WARNING
            is AbstractImplementationException -> SentryLevel.ERROR
            else -> SentryLevel.FATAL
        }

        private fun exceptionToSentryStatus(throwable: Throwable) = when (throwable) {
            is GrantException -> SpanStatus.PERMISSION_DENIED
            is AbstractUserException -> SpanStatus.INVALID_ARGUMENT
            is AbstractImplementationException -> SpanStatus.UNIMPLEMENTED
            else -> SpanStatus.INTERNAL_ERROR
        }

        fun createPerf(name: String, featureFlag: Boolean, user: User? = null): ITransaction? {
            if (!featureFlag) return null
            if (ConfigHandler.config.sentryDsn == null) throw MissingSentryDSN()
            if (user != null) {
                Sentry.setUser(toSentryUser(user))
            }
            return if (ConfigHandler.config.timeCronExecutionsInSentry) {
                Sentry.startTransaction(name, "task")
            } else {
                null
            }
        }

        fun addException(transaction: ITransaction?, e: Exception) {
            if (transaction != null) {
                transaction.throwable = e
                Sentry.configureScope { it.level = exceptionToSentryLevel(e) }
                transaction.status = exceptionToSentryStatus(e)
            }
        }

        fun configureScope(callback: ScopeCallback) {
            if (ConfigHandler.config.sentryDsn == null) return
            Sentry.configureScope(callback)
        }
    }
}
