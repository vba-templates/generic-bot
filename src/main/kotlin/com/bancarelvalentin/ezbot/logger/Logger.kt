package com.bancarelvalentin.ezbot.logger

import com.bancarelvalentin.ezbot.config.ConfigHandler
import com.bancarelvalentin.ezbot.exception.DiscordBotException
import com.bancarelvalentin.ezbot.exception.DiscordErrorLogger
import com.bancarelvalentin.ezbot.exception.implerrors.UnknownErrorWrapper
import io.sentry.Sentry
import net.dv8tion.jda.api.entities.User
import org.slf4j.Logger
import java.util.UUID

@Suppress("unused")
class Logger(javaClass: Class<*> = Logger::class.java) {

    private val uuid: UUID = UUID.randomUUID()
    private val logger = LogbackConfig.get(javaClass)

    fun trace(str: String, cat: LogCategory = LogCategoriesEnum.DEFAULT, throwable: Throwable? = null, user: User? = null) {
        if (throwable == null) {
            logger.trace(getMessage(str, cat))
        } else {
            logThrowable(throwable, user)
            logger.trace(getMessage(str, cat), throwable)
        }
    }

    fun debug(str: String, cat: LogCategory = LogCategoriesEnum.DEFAULT, throwable: Throwable? = null, user: User? = null) {
        if (throwable == null) {
            logger.debug(getMessage(str, cat))
        } else {
            logThrowable(throwable, user)
            logger.debug(getMessage(str, cat), throwable)
        }
    }

    fun info(str: String, cat: LogCategory = LogCategoriesEnum.DEFAULT, throwable: Throwable? = null, user: User? = null) {
        if (throwable == null) {
            logger.info(getMessage(str, cat))
        } else {
            logThrowable(throwable, user)
            logger.info(getMessage(str, cat), throwable)
        }
    }

    fun warn(str: String, cat: LogCategory = LogCategoriesEnum.DEFAULT, throwable: Throwable? = null, user: User? = null) {
        if (throwable == null) {
            logger.warn(getMessage(str, cat))
        } else {
            logThrowable(throwable, user)
            logger.warn(getMessage(str, cat), throwable)
        }
    }

    fun error(str: String, cat: LogCategory = LogCategoriesEnum.DEFAULT, throwable: Throwable? = null, user: User? = null) {
        if (throwable == null) {
            logger.error(getMessage(str, cat))
        } else {
            logThrowable(throwable, user)
            logger.error(getMessage(str, cat), throwable)
        }
    }

    private fun getMessage(str: String, cat: LogCategory): String {
        var strCat = cat.code
        while (strCat.length < 30) {
            strCat += " "
        }

        return if (cat == LogCategoriesEnum.DEFAULT) {
            "$str "
        } else {
            "[$strCat] - $str "
        }
    }

    private fun logThrowable(throwable: Throwable, user: User?) {
        if (ConfigHandler.config.logErrorsOnSentry) {
            if (user != null) {
                val userS: io.sentry.protocol.User = io.sentry.protocol.User()
                userS.username = user.name
                userS.id = user.id
                Sentry.setUser(userS)
            }
            Sentry.captureException(throwable)
        }
        try {
            val asDiscordError =
                if (throwable is DiscordBotException) throwable
                else UnknownErrorWrapper(throwable)
            DiscordErrorLogger.handle(asDiscordError)
        } catch (_: Throwable) {
        }
    }
}
