package com.bancarelvalentin.ezbot.logger

import ch.qos.logback.classic.Logger
import ch.qos.logback.classic.LoggerContext
import ch.qos.logback.classic.encoder.PatternLayoutEncoder
import ch.qos.logback.classic.spi.ILoggingEvent
import ch.qos.logback.core.Appender
import ch.qos.logback.core.ConsoleAppender
import ch.qos.logback.core.rolling.RollingFileAppender
import ch.qos.logback.core.rolling.SizeAndTimeBasedRollingPolicy
import ch.qos.logback.core.util.FileSize
import com.bancarelvalentin.ezbot.config.ConfigHandler
import com.bancarelvalentin.ezbot.config.EnvConfig
import org.slf4j.Logger.ROOT_LOGGER_NAME
import org.slf4j.LoggerFactory

class LogbackConfig {
    companion object {

        private val logCtx: LoggerContext = LoggerFactory.getILoggerFactory() as LoggerContext
        private val appenders = ArrayList<Appender<ILoggingEvent>>()
        private var additive: Boolean = false
        private var ezBotLevel = ConfigHandler.config.appLogLevel
        private var rootLevel = ConfigHandler.config.rootLogLevel

        init {
            if (EnvConfig.DEV)
                configureDev()
            else
                configureDefault()
            Logger(LogbackConfig::class.java).info("Logback configured programmatically. Please use ${com.bancarelvalentin.ezbot.logger.Logger::class.java.name} to create new loggers.", LogCategoriesEnum.INITIAL_STARTUP)
        }

        fun get(clazz: Class<*>): Logger {
            val logger = logCtx.getLogger(clazz)
            setup(logger)
            return logger
        }

        private fun setup(logger: Logger) {
            logger.loggerContext
            logger.isAdditive = additive
            logger.level = ezBotLevel
            appenders.forEach { logger.addAppender(it) }
        }

        private fun configureDev() {
            configureDefault()
        }

        private fun configureDefault() {
            val root = LoggerFactory.getLogger(ROOT_LOGGER_NAME) as Logger
            root.level = rootLevel

            val logConsoleAppender = ConsoleAppender<ILoggingEvent>()
            logConsoleAppender.context = logCtx
            logConsoleAppender.name = "STDOUT"
            logConsoleAppender.encoder = getDefaultEncoder(logCtx)
            logConsoleAppender.start()

            val logFileAppender = RollingFileAppender<ILoggingEvent>()
            logFileAppender.context = logCtx
            logFileAppender.name = "FILE-ROLLING"
            logFileAppender.encoder = getDefaultEncoder(logCtx)
            logFileAppender.file = "/log/bot.log"
            logFileAppender.rollingPolicy = getDefaultRollingFile(logCtx, logFileAppender)
            logFileAppender.start()

            appenders.add(logConsoleAppender)
            appenders.add(logFileAppender)
        }

        private fun getDefaultRollingFile(@Suppress("SameParameterValue") logCtx: LoggerContext, logFileAppender: RollingFileAppender<ILoggingEvent>): SizeAndTimeBasedRollingPolicy<ILoggingEvent> {
            val logFilePolicy = SizeAndTimeBasedRollingPolicy<ILoggingEvent>()
            logFilePolicy.context = logCtx
            logFilePolicy.setParent(logFileAppender)
            logFilePolicy.fileNamePattern = "/log/archived/bot.%d{yyyy-MM-dd}.%i.log.gz"
            logFilePolicy.setMaxFileSize(FileSize.valueOf("5MB"))
            logFilePolicy.setTotalSizeCap(FileSize.valueOf("100MB"))
            logFilePolicy.maxHistory = 120
            logFilePolicy.start()
            return logFilePolicy
        }

        private fun getDefaultEncoder(@Suppress("SameParameterValue") logCtx: LoggerContext): PatternLayoutEncoder {
            val logEncoder = PatternLayoutEncoder()
            logEncoder.context = logCtx
            logEncoder.pattern = "%d{yyyy-MM-dd'T'HH:mm:ss.SSS} [EzBot] [%-30logger{0}] [%-5level] - %msg%n"
            logEncoder.start()
            return logEncoder
        }
    }
}
