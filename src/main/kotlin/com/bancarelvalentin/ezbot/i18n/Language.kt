package com.bancarelvalentin.ezbot.i18n

class Language(val key: String, val name: String? = null, val isDefault: Boolean? = null)
