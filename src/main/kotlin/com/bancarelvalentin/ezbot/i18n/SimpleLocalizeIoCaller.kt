package com.bancarelvalentin.ezbot.i18n

import com.bancarelvalentin.ezbot.config.ConfigHandler
import com.bancarelvalentin.ezbot.exception.implerrors.i18n.SimpleLocalizeException
import com.bancarelvalentin.ezbot.exception.implerrors.i18n.SimpleLocalizeHttpCallErrorException
import com.bancarelvalentin.ezbot.logger.Logger
import com.bancarelvalentin.ezbot.utils.Constants
import org.json.JSONArray
import org.json.JSONObject
import java.io.BufferedReader
import java.io.InputStream
import java.io.InputStreamReader
import java.net.HttpURLConnection
import java.net.URL

class SimpleLocalizeIoCaller {

    companion object {

        private val logger = Logger(SimpleLocalizeIoCaller::class.java)

        private fun post(url: String, payload: String, @Suppress("SameParameterValue") authed: Boolean = false): String {

            return http(url, "POST") {
                if (authed)
                    it.setRequestProperty("X-SimpleLocalize-Token", ConfigHandler.config.simpleLocalizeApiKey!!)

                it.setRequestProperty("Content-Type", "application/json; utf-8")
                it.setRequestProperty("Content-Length", "" + payload.toByteArray().size)
                it.doOutput = true

                it.outputStream.use { os ->
                    val input = payload.toByteArray()
                    os.write(input, 0, input.size)
                }
            }
        }

        private fun get(urlStr: String, authed: Boolean = false): String {
            return http(urlStr, "GET") {
                if (authed)
                    it.setRequestProperty("X-SimpleLocalize-Token", ConfigHandler.config.simpleLocalizeApiKey!!)
            }
        }

        private fun http(urlStr: String, method: String, connectionSetter: ((HttpURLConnection) -> Unit)? = null): String {
            val url = URL(urlStr)
            val con: HttpURLConnection = url.openConnection() as HttpURLConnection
            con.requestMethod = method
            connectionSetter?.invoke(con)

            logger.trace("[${con.responseCode}] - $url")
            if (con.errorStream != null) {
                throw SimpleLocalizeException(read(con.errorStream))
            } else if (con.inputStream != null) {
                return read(con.inputStream)
            } else {
                throw SimpleLocalizeHttpCallErrorException()
            }
        }

        private fun read(stream: InputStream): String {
            val reader = BufferedReader(InputStreamReader(stream))
            var inputLine: String?
            val content = StringBuffer()
            while (reader.readLine().also { inputLine = it } != null) {
                content.append(inputLine)
            }
            reader.close()
            return content.toString()
        }

        private fun httpToObject(url: String): JSONObject {
            return JSONObject(get(url))
        }

        private fun httpToArray(url: String): JSONArray {
            return JSONArray(get(url))
        }

        @Suppress("unused")
        fun getCustomers(token: String): Array<Customer> {
            return httpToArray(getCdnUrl(token, "_customers")).map {
                it as JSONObject
                Customer(it.getString("id"))
            }.toTypedArray()
        }

        fun getLanguages(token: String): Array<Language> {
            return httpToArray(getCdnUrl(token, "_languages")).map {
                it as JSONObject
                Language(it.getString("key"), it.getString("name"), it.getBoolean("isDefault"))
            }.toTypedArray()
        }

        fun getIndexes(token: String): Map<String, I18nIndex> {
            val map = HashMap<String, I18nIndex>()
            val jsonObject = httpToObject(getCdnUrl(token, "_index"))
            for (key in jsonObject.keySet()) {
                map[key] = fillIndex(jsonObject.getJSONObject(key), I18nIndex(Language(key)))
            }
            return map
        }

        @Suppress("unused")
        fun getIndex(token: String, lang: Language): I18nIndex {
            return fillIndex(httpToObject(getCdnUrl(token, lang.key)), I18nIndex(lang))
        }

        private fun getCdnUrl(token: String, vararg paths: String): String {
            val env = "_latest"
            val path = paths.toList().joinToString("/")
            return "${Constants.SIMPLE_LOCALIZE_CDN_URL}/$token/$env/$path"
        }

        private fun getApiUrl(vararg paths: String): String {
            val path = paths.toList().joinToString("/")
            return "${Constants.SIMPLE_LOCALIZE_API_URL}/translations/$path"
        }

        private fun fillIndex(json: JSONObject, pI18nIndex: I18nIndex): I18nIndex {
            for (key in json.keySet()) {
                pI18nIndex[key] = json.getString(key)
            }
            return pI18nIndex
        }

        fun addKey(key: String, lang: String) {
            val json = JSONObject()
            json.put("key", key)
            json.put("language", lang)
            post(getApiUrl(), JSONObject().put("content", JSONArray().put(json)).toString(), true)
            logger.info("Property key $lang/'$key' added to simplelocalize.io")
        }
    }
}
