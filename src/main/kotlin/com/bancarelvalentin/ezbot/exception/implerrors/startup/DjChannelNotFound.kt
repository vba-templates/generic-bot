package com.bancarelvalentin.ezbot.exception.implerrors.startup

import com.bancarelvalentin.ezbot.exception.implerrors.AbstractImplementationException

class DjChannelNotFound : AbstractImplementationException("Channel for DJ actions not found")
