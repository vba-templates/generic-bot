package com.bancarelvalentin.ezbot.exception.implerrors.commands

import com.bancarelvalentin.ezbot.exception.implerrors.AbstractImplementationException
import com.bancarelvalentin.ezbot.process.command.Command

class CommandLogicException(command: Command, cause: Exception) : AbstractImplementationException("An exception occcured in a '${command.rawName}' command", cause)
