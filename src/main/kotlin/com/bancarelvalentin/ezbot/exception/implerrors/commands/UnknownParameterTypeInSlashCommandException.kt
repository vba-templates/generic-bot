package com.bancarelvalentin.ezbot.exception.implerrors.commands

import com.bancarelvalentin.ezbot.exception.implerrors.AbstractImplementationException
import net.dv8tion.jda.api.interactions.commands.OptionType

class UnknownParameterTypeInSlashCommandException(type: OptionType) : AbstractImplementationException("A parameter sent via a Slash command had an unknown type (${type.name})")
