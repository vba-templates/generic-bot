package com.bancarelvalentin.ezbot.exception.implerrors.startup

import com.bancarelvalentin.ezbot.exception.implerrors.AbstractImplementationException

class MissingSimpleLocalizeApiKey : AbstractImplementationException("You need to provide an API key in order to create missing simple localize strings")
