package com.bancarelvalentin.ezbot.exception.implerrors.startup

import com.bancarelvalentin.ezbot.exception.implerrors.AbstractImplementationException

class CantSupportAnyOtherCommandsTypeWithoutChatCommands : AbstractImplementationException("If you activate slash or lazy commands, chat commands must be activated as well")
