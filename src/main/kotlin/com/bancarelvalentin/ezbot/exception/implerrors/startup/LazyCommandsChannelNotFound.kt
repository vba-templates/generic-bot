package com.bancarelvalentin.ezbot.exception.implerrors.startup

import com.bancarelvalentin.ezbot.exception.implerrors.AbstractImplementationException

class LazyCommandsChannelNotFound : AbstractImplementationException("Channel for lazy command not found")
