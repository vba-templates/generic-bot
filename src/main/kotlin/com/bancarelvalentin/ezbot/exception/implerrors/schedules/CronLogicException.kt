package com.bancarelvalentin.ezbot.exception.implerrors.schedules

import com.bancarelvalentin.ezbot.exception.implerrors.AbstractImplementationException
import com.bancarelvalentin.ezbot.process.scheduled.ScheduledProcess

class CronLogicException(cron: ScheduledProcess, cause: Exception) : AbstractImplementationException("An exception occcured in a '${cron.rawName}' CRON", cause)
