package com.bancarelvalentin.ezbot.exception.implerrors.orchestrator

import com.bancarelvalentin.ezbot.exception.implerrors.AbstractImplementationException
import com.bancarelvalentin.ezbot.process.Process

class ProcessNotFoundException(clazz: Class<out Process>) : AbstractImplementationException("Process $clazz not found in current running processes")
