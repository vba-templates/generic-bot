package com.bancarelvalentin.ezbot.exception.implerrors.startup

import com.bancarelvalentin.ezbot.exception.implerrors.AbstractImplementationException

class MissingSentryDSN : AbstractImplementationException("You need to provide a DSN for sentry to work")
