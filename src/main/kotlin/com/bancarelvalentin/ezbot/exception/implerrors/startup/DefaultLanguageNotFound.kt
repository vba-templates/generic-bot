package com.bancarelvalentin.ezbot.exception.implerrors.startup

import com.bancarelvalentin.ezbot.exception.implerrors.AbstractImplementationException

class DefaultLanguageNotFound : AbstractImplementationException("Default language not set via simplelocalized.io or the code")
