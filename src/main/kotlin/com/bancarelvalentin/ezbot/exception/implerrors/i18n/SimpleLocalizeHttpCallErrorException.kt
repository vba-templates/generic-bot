package com.bancarelvalentin.ezbot.exception.implerrors.i18n

import com.bancarelvalentin.ezbot.exception.implerrors.AbstractImplementationException

class SimpleLocalizeHttpCallErrorException : AbstractImplementationException("It seems an http call had no response")
