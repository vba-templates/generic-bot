package com.bancarelvalentin.ezbot.exception.implerrors.commands

import com.bancarelvalentin.ezbot.exception.implerrors.AbstractImplementationException
import net.dv8tion.jda.api.events.message.GenericMessageEvent

class UnknownChatCommandMessageType(clazz: Class<out GenericMessageEvent>) : AbstractImplementationException("A message event was neither a MessageReceivedEvent nor a MessageUpdateEvent (${clazz.simpleName})")
