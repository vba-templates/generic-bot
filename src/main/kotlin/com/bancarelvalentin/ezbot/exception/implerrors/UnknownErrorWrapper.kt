package com.bancarelvalentin.ezbot.exception.implerrors

class UnknownErrorWrapper(cause: Throwable) : AbstractImplementationException("Erreur inconnue", cause)
