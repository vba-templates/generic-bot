package com.bancarelvalentin.ezbot.exception.implerrors.startup

import com.bancarelvalentin.ezbot.exception.implerrors.AbstractImplementationException

class BotAlreadyStartedException : AbstractImplementationException("Bot has already been started")
