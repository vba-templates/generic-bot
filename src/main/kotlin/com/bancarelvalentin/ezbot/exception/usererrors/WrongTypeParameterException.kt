package com.bancarelvalentin.ezbot.exception.usererrors

import com.bancarelvalentin.ezbot.i18n.DefaultI18n
import com.bancarelvalentin.ezbot.i18n.Localizator

class WrongTypeParameterException(paramName: String, paramType: String, position: Int) : AbstractUserException(Localizator.get(DefaultI18n.USER_ERROR_WRONG_PARAM_TYPE, paramName, position, paramType))
