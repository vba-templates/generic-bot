package com.bancarelvalentin.ezbot.exception.usererrors.player

import com.bancarelvalentin.ezbot.exception.usererrors.AbstractUserException
import com.sedmelluq.discord.lavaplayer.tools.FriendlyException

class UserToFiendlyException(e: AbstractUserException) : FriendlyException(e.message, Severity.COMMON, e)
