package com.bancarelvalentin.ezbot.exception.usererrors.player

import com.bancarelvalentin.ezbot.exception.usererrors.AbstractUserException
import com.bancarelvalentin.ezbot.i18n.DefaultI18n
import com.bancarelvalentin.ezbot.i18n.Localizator

class MustBeInAVoiceChannel : AbstractUserException(Localizator.get(DefaultI18n.USER_ERROR_MUST_BE_IN_A_VOICE_CHANNEL))
