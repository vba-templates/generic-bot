package com.bancarelvalentin.ezbot.exception.usererrors

import com.bancarelvalentin.ezbot.i18n.DefaultI18n
import com.bancarelvalentin.ezbot.i18n.Localizator

class MissingRequiredParameterException(paramName: String, position: Int) : AbstractUserException(Localizator.get(DefaultI18n.USER_ERROR_MISSING_PARAM, paramName, position))
