package com.bancarelvalentin.ezbot.exception.usererrors.player

import com.bancarelvalentin.ezbot.exception.usererrors.AbstractUserException
import com.bancarelvalentin.ezbot.i18n.DefaultI18n
import com.bancarelvalentin.ezbot.i18n.Localizator

class NoResults : AbstractUserException(Localizator.get(DefaultI18n.USER_ERROR_NO_PLAYER_RESULTS))
