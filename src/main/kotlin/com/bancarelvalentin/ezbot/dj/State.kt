package com.bancarelvalentin.ezbot.dj

enum class State {
    STOPPED,
    PLAYING,
    PAUSED,
}
