package com.bancarelvalentin.ezbot.dj

import com.bancarelvalentin.ezbot.exception.DiscordBotException
import com.bancarelvalentin.ezbot.exception.usererrors.AbstractUserException
import com.bancarelvalentin.ezbot.exception.usererrors.player.NoResults
import com.bancarelvalentin.ezbot.exception.usererrors.player.PlayerSafeErrorWrapper
import com.bancarelvalentin.ezbot.exception.usererrors.player.PlayerUnsafeErrorWrapper
import com.bancarelvalentin.ezbot.exception.usererrors.player.UserToFiendlyException
import com.sedmelluq.discord.lavaplayer.player.AudioLoadResultHandler
import com.sedmelluq.discord.lavaplayer.tools.FriendlyException
import com.sedmelluq.discord.lavaplayer.track.AudioPlaylist
import com.sedmelluq.discord.lavaplayer.track.AudioTrack
import com.sedmelluq.discord.lavaplayer.track.AudioTrackInfo
import net.dv8tion.jda.api.entities.AudioChannel

class SearchResultHandler(
    private val channel: AudioChannel?,
    private val addFirst: Boolean,
    private val onSuccess: ((Array<AudioTrackInfo>) -> Unit)? = null,
    private val onError: ((DiscordBotException) -> Unit)?,
    private val onLoad: (Boolean, AudioChannel?, Array<AudioTrack>) -> Unit,
) : AudioLoadResultHandler {
    override fun trackLoaded(track: AudioTrack) {
        val array = arrayOf(track)
        genericSuccess(array)
    }

    override fun playlistLoaded(playlist: AudioPlaylist) {
        val array = playlist.tracks.toTypedArray()
        genericSuccess(array)
    }

    private fun genericSuccess(array: Array<AudioTrack>) {
        try {
            onLoad.invoke(addFirst, channel, array)
            onSuccess?.invoke(array.map { it.info }.toTypedArray())
        } catch (e: AbstractUserException) {
            loadFailed(UserToFiendlyException(e))
        }
    }

    override fun noMatches() {
        onError?.invoke(NoResults())
    }

    override fun loadFailed(friendlyException: FriendlyException) {
        if (friendlyException.severity == FriendlyException.Severity.COMMON) {
            onError?.invoke(PlayerSafeErrorWrapper(friendlyException))
        } else {
            onError?.invoke(PlayerUnsafeErrorWrapper(friendlyException))
        }
    }
}
